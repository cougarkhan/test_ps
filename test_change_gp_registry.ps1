#$machines = gc A:\Temp\sm_pc.txt
$machines = "supman-29"

foreach ($pc in $machines) {
    try {
    $result = gwmi -class Win32_PingStatus -filter "Address='$pc'"
    } catch {
        Write-Host $pc not Online
    }
    
    if ($result.StatusCode -eq 0) {
        $reg = [Microsoft.Win32.RegistryKey]::OpenRemoteBaseKey('LocalMachine', $pc)
        $regKey= $reg.OpenSubKey("System\CurrentcontrolSet\Control\Print\Providers\LanMan Print Services\Servers",$true)
        $prev = $regKey.GetValue("AddPrinterDrivers")
        $regKey.SetValue("AddPrinterDrivers","1")
        $cur = $regKey.GetValue("AddPrinterDrivers")
        Write-Host Changed AddPrinterDrivers for $pc from $prev to $cur
    }
}