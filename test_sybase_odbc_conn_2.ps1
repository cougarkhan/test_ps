$sa_conn = new-object System.Data.Odbc.OdbcConnection
$sa_cmd = new-object System.Data.Odbc.OdbcCommand
$sa_conn.ConnectionString = "Driver=Adaptive Server Anywhere 9.0;ENG=bis-240_db_server;UID=scan;PWD=scan; DBF=C:\Temp\sa_scan\sa_scan.db;"
$sa_conn.Open()
$sa_cmd.Connection = $sa_conn
#$sa_query = "SELECT * FROM scan.table_acs_credential_log WHERE change_date_time is not NULL and change_date_time > '2013-08-28 12:39:00.000' ORDER BY change_date_time;"
#$sa_query = "SELECT * FROM scan.table_acs_credential_log;"
#$sa_query = "SELECT Credential_ID, count(Credential_ID) from scan.table_acs_credential_log WHERE change_date_time < '2013-08-28%' AND IN_Status is Null OR IN_Status = '' GROUP BY Credential_ID having count(*) > 10;"
$sa_query = "SELECT hdr.ord_Scan,hdr.ord_a3_rpt_hdr,hdr.date_generated,hdr.date_last_cleared,hdr.ord_dev,hdr.period,hdr.lane_number,hdr.rpt_type,hdr.cleared,tax.tax_indx,tax.tax_txn_count,tax.taxable_revenue,tax.tax_collected,mike.tax_name,mike.tax_type,mike.add_on,mike.tax_on_tax,mike.tax_amount FROM table_a3_rpt_hdr as hdr,table_a3_rpt_tax as tax,table_a3_rpt_tax_info as mike WHERE hdr.ord_Scan = tax.ord_Scan AND hdr.ord_a3_rpt_hdr = tax.ord_a3_rpt_hdr AND tax.ord_Scan = mike.ord_Scan AND tax.ord_a3_rpt_tax_info = mike.ord_a3_rpt_tax_info AND tax.tax_indx = mike.tax_indx AND hdr.cleared <> 0 AND hdr.rpt_type = 108 AND hdr.period = 0 order by tax_indx ASC"
$sa_cmd.CommandText = $sa_query
$sa_adapter = New-Object System.Data.odbc.OdbcDataAdapter
$sa_adapter.SelectCommand=$sa_cmd
$sa_dataset = New-Object System.Data.DataSet
$sa_dataset.clear()
measure-command { $sa_adapter.Fill($sa_dataset) }
$sa_conn.close()

# $bak_conn = new-object System.Data.Odbc.OdbcConnection
# $bak_cmd = new-object System.Data.Odbc.OdbcCommand
# $bak_conn.ConnectionString = "Driver=Adaptive Server Anywhere 9.0;ENG=bis-240_db_server;UID=scan;PWD=scan; DSN=bak_scan; DBF=C:\Temp\sa_scan\bak_scan.db;"
# $bak_conn.Open()
# $bak_cmd.Connection = $bak_conn
# $bak_query = "SELECT * FROM scan.table_acs_credential_log WHERE change_date_time is not NULL and change_date_time like '2013-08-27%' ORDER BY change_date_time;"
# $bak_query = "SELECT * FROM scan.table_acs_credential_log;"
# $bak_cmd.CommandText = $bak_query
# $bak_adapter = New-Object System.Data.odbc.OdbcDataAdapter
# $bak_adapter.SelectCommand=$bak_cmd
# $bak_dataset = New-Object System.Data.DataSet
# $bak_dataset.clear()
# $bak_adapter.Fill($bak_dataset)
# $bak_conn.close()


#$cmd.ExecuteNonQuery()
#$cmd.ExecuteScalar()
#return $dataset

#$conn.ConnectionString = "Driver=Adaptive Server Anywhere 9.0;ENG=bops-sn-tst_db_server;UID=scan;PWD=scan; DBN=sa_scan;LINKS=TCPIP(HOST=142.103.108.198);"