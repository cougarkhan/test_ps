﻿param ([string]$ns, [string]$nd, [string]$s, [string]$t, [string]$l)

#############################################################################
# Load SQL Server assemblies
#############################################################################

[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.SMO") | Out-Null
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.SmoExtended") | Out-Null
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.ConnectionInfo") | Out-Null
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.SmoEnum") | Out-Null


function write_log {
	param ([string]$logPath, [string] $entry, [bool]$append=$true)
	
	if (!(Test-Path $logPath)) {
		$log = New-Item -Path $logPath -type File -Force
	} else {
		$log = (gi $logPath)
	}
	
	if ($append -eq $true) {
		$entry | Out-File -FilePath $log -Append
	} else {
		$entry | Out-File -FilePath $log
	}
	
}

function backup_db {
	param ([string]$server, [string]$database, [string]$backupDir)
	
	$sqlServer = New-Object ("Microsoft.SqlServer.Management.Smo.Server") $server
	$sqlServer = New-Object ("Microsoft.SqlServer.Management.Smo.Server") $server
	
	$timestamp = Get-Date -Format yyyyMMddHHmmss
	
	$db = $sqlServer.Databases[$database]
	$dbName = $db.Name
	
	$backupFile = $backupDir + "\" + $dbName + "_" + $timestamp + ".bak"
	$backupFile
				
	$smoBackup = New-Object ("Microsoft.SqlServer.Management.Smo.Backup")
	 
	$smoBackup.Action = "Database"
	$smoBackup.BackupSetDescription = "Full Backup of " + $dbName
	$smoBackup.BackupSetName = $dbName + " Backup"
	$smoBackup.Database = $dbName
	$smoBackup.MediaDescription = "Disk"
	$smoBackup.Devices.AddDevice($backupDir + "\" + $dbName + "_" + $timestamp + ".bak", "File")
	$smoBackup.SqlBackup($sqlServer)


	$logOut = "Backup Dir: " + $backupDir + "\" + $dbName + "_" + $timestamp + ".bak"
	
	write_log $l $logOut
	
}

function drop_db {
	param ([string]$server, [string]$database)
	
	$sqlServer = New-Object ("Microsoft.SqlServer.Management.Smo.Server") $server
	$db = $sqlServer.Databases[$database]
	$db.DatabaseOptions.UserAccess = "Single"
	$sqlServer.KillAllProcesses($database)
	$db.drop()
	
	if ($db.alter()) {
		return $true 
	} else {
		return $false 
	}

}

function restore_db {
	param ([string]$server, [string]$backupFile)
			
	$sqlServer = New-Object ("Microsoft.SqlServer.Management.Smo.Server") $server
	$backupDevice = New-Object("Microsoft.SqlServer.Management.Smo.BackupDeviceItem") ($backupFile, "File")
	
	$smoRestore = new-object("Microsoft.SqlServer.Management.Smo.Restore")
	
	$smoRestore.NoRecovery = $false;
	$smoRestore.ReplaceDatabase = $true;
	$smoRestore.Action = "Database"
	$smoRestorePercentCompleteNotification = 10;
	$smoRestore.Devices.Add($backupDevice)
		 
	$smoRestoreDetails = $smoRestore.ReadBackupHeader($sqlServer)
	 
	"Database Name from Backup Header : " + $smoRestoreDetails.Rows[0]["DatabaseName"]
	 
	$smoRestore.Database = $smoRestoreDetails.Rows[0]["DatabaseName"]
	 
	$smoRestoreFile = New-Object("Microsoft.SqlServer.Management.Smo.RelocateFile")
	$smoRestoreLog = New-Object("Microsoft.SqlServer.Management.Smo.RelocateFile")
	 
	$smoRestoreFile.LogicalFileName = $smoRestoreDetails.Rows[0]["DatabaseName"]
	$smoRestoreFile.PhysicalFileName = $sqlServer.Information.MasterDBPath + "\" + $smoRestore.Database + "_Data.mdf"
	$smoRestoreLog.LogicalFileName = $smoRestoreDetails.Rows[0]["DatabaseName"] + "_Log"
	$smoRestoreLog.PhysicalFileName = $sqlServer.Information.MasterDBLogPath + "\" + $smoRestore.Database + "_Log.ldf"
	$smoRestore.RelocateFiles.Add($smoRestoreFile)
	$smoRestore.RelocateFiles.Add($smoRestoreLog)
	 
	drop_db $sqlServer $smoRestoreDetails.Rows[0]["DatabaseName"]
	$smoRestore.SqlRestore($sqlServer)
}

function move_files {
	param ([string]$source, [string]$target)

	gci $target | foreach { write_log $l ($_.FullName + " Deleted."); Remove-Item $_.FullName -Force}
	gci $source | foreach { write_log $l ($_.FullName + " Moved to " + $target); Move-Item $_.FullName ($target + "\$_") -Force}
}
[array]$dsxDbs = ("AcsData","AcsLog","DSX-SystemSettings","DSX-Update")

$dsxDbs | foreach { backup_db $ns $_ "E:\Refresh" }
move_files $s $t
gci $t | foreach { restore_db $nd $_.FullName }
