$ion_list = import-csv "C:\Temp\ion\Device IP addresses(January2014).csv"

$sqlServer = "localhost\local"
$sqlDatabase = "ubc_prod"
$sqlConn = New-Object System.Data.SqlClient.SqlConnection
$sqlConn.ConnectionString = "Server = $sqlServer; Database = $sqlDatabase; Integrated Security = True"
$sqlCmd = New-Object System.Data.SqlClient.SqlCommand
$sqlCmd.Connection = $sqlConn

$result = @()

$ion_list | Add-Member -type NoteProperty -name Archibus_Name -value ""
$ion_list | Add-Member -type NoteProperty -name Archibus_Short_Name -value ""

foreach ($ion in $ion_list)
{
	$id = $ion.Building_ID
	$sqlQuery1 = "
		SELECT 
			[afm].[bl].bl_id as Building_ID, 
			[afm].[bl].option1 as Group_ID,
			[afm].[bl].name as Name,
			[afm].[bl_shortnames].short_name as Short_Name
		FROM
			[afm].[bl]
		INNER JOIN 
			[afm].[bl_shortnames]
		ON
			[afm].[bl].bl_id = [afm].[bl_shortnames].bl_id
		WHERE
			[afm].[bl].bl_id = '$id';"
	$sqlCmd.CommandText = $sqlQuery1
	$sqlAdapter = New-Object System.Data.SqlClient.SqlDataAdapter
	$sqlAdapter.SelectCommand = $sqlCmd
	$dataSet = New-Object System.Data.DataSet
	$sqlAdapter.Fill($dataSet,"Buildings") | Out-Null
	
	foreach ($building in $dataSet.Tables["Buildings"])
	{
		$ion.Archibus_Name = ($building.Name).Trim()
		$ion.Archibus_Short_Name = ($building.Short_Name).Trim()
	}
	$sqlAdapter.Dispose()
}

$ion_list | export-csv C:\temp\ion\complete.csv