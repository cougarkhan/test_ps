param ([string]$server)

$servers = Get-ADComputer -filter {adminDescription -like "*domain*"} -Properties admindescription | select -exp dnshostname

$nwac = gwmi -class win32_networkadapterconfiguration -computername $servers | where {$_.IPAddress -ne $null} | select DNSHostName,IPAddress,IPSubnet,DefaultIPGateway,MACAddress,DNSDomain,DNSDomainSuffixSearchOrder

foreach ($item in $nwac) {
	$item
}