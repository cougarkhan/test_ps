param ([string]$name, [string]$filePath, [switch]$recurse, [switch]$check, [switch]$ls, [switch]$applied)

#start-transcript C:\temp\SUPMAN_ACL_SM-ADMIN_ADD.log

if ($name -eq "") { 
	write-host "Username missing."
	exit
} 
if ($filePath -eq "") {
	write-host "File Path missing."
	exit
}

Import-Module ActiveDirectory

$userName = get-adobject -filter "sAMAccountName -eq '$name'" -Properties objectSid
$permission = "BUSSOPS\SM-Admin","FullControl","ContainerInherit,ObjectInherit","None","Allow"
$accessRule = new-object System.Security.AccessControl.FileSystemAccessRule $permission
[array]$fileList = @()

write-host "Building File List..." -nonewline
if ($ls -eq $true) {
	if ($recurse -eq $true) {
		$fileList = ls $filePath -directory -recurse
	} else {
		$fileList = ls $filePath -directory
	}
} else {
	$fileList = gc $filePath
}
write-host "Done`n`n" -nonewline

$userSID = $userName.objectSid.Value
$max = $fileList.count
$counter = 1

foreach ($item in $fileList) {
	[int]$pc = ($counter / $max) * 100
	write-progress -activity "Checking..." -currentoperation "$pc% $item" -status "Please Wait." -percentcomplete $pc
	$aclFound = $false
	if ((test-path $item) -eq $true) {
		$itemACL = (get-item $item -force).GetAccessControl("Access")
		foreach($access in $itemACL.Access) {
			$SID = $access.IdentityReference.Translate([System.Security.Principal.SecurityIdentifier])

			if ($SID.BinaryLength -ge 28)
			{
				$itemSID = $SID.Value
				
				if ($itemSID -eq $userSID) {
					$aclFound = $true
				}
			}
		}
		
		if ($aclFound -eq $false -AND $applied -eq $false) {
			if ($check -eq $false) {
				$itemACL.AddAccessRule($accessRule)
				write-host "Adding $permission TO $item"
				$itemACL | set-acl $item
			} else {
				$item
			}
		}
		if ($aclFound -eq $true -AND $applied -eq $true) {
			$item
		}
	}
	
	$counter = $counter + 1
}

#stop-transcript