$path = "E:\Microsoft SQL Server\MSSQL10_50.PROTEGEGX\MSSQL\DATA\"
$dbPath = "SQLSERVER:\sql\bis-sqlict-tst.bussops.ubc.ca\protegegx\databases"
$databases = ls $dbPath

$rfl = @()

foreach ($database in $databases)
{
	$name = $database.name
	$instace = $database.Parent
	$data = new-object Microsoft.SqlServer.Management.Smo.RelocateFile
	$log = new-object Microsoft.SqlServer.Management.Smo.RelocateFile
	$data.LogicalFileName = $database.name
	$log.LogicalFileName = "$($database.name)_log"
	$data.PhysicalFileName = "$($database.PrimaryFilePath)\$($database.name).mdf"
	$log.PhysicalFileName = "$($database.PrimaryFilePath)\$($database.name).ldf"
	
	$rfl += $data
	$rfl += $log
	
	#$backupFile = (ls "F:\Backup\$($name)" | where { $_.creationtime.day -eq $((date).day - 1) }).fullname
	
	#$database.drop()
	
	#Restore-SqlDatabase -ServerInstance $instance -Database $name -BackupFile $backupFile -RelocateFile $rfl
}


$path = "E:\Microsoft SQL Server\MSSQL10_50.PROTEGEGX\MSSQL\DATA\"
$names = ("ArmorIP","ArmorIPEvents","ProtegeGX","ProtegeGXEvents")

foreach ($name in $names)
{
	$instance = "BIS-SQLICT-TST\ProtegeGX"
	$data = new-object Microsoft.SqlServer.Management.Smo.RelocateFile
	$log = new-object Microsoft.SqlServer.Management.Smo.RelocateFile
	$data.LogicalFileName = $name
	$log.LogicalFileName = "$($name)_log"
	$data.PhysicalFileName = "$($path)$($name).mdf"
	$log.PhysicalFileName = "$($path)$($name).ldf"
	
	$rfl = @()
	$rfl += $data
	$rfl += $log
	
	$backupFile = (ls "F:\Backup\$($name)" | where { $_.creationtime.day -eq $((date).day - 1) }).fullname
	
	Restore-SqlDatabase -ServerInstance $instance -Database $name -BackupFile $backupFile -RelocateFile $rfl
}


