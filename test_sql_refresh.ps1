$ErrorActionPreference = "Stop"

$srcInst = "BIS-SQLICT-PRD"
$dstInst = "BIS-SQLICT-DEV"

function logTime 
{
	#return (get-date).ToLongTimeString()
	return Get-Date -Format HH:mm:ss
}

try
{
	write-host (logTime)"Connecting to $srcInst..." -nonewline
	$srcSrv = new-object ('Microsoft.SqlServer.Management.Smo.Server') $srcInst
	$srcDatabases = $srcSrv.Databases
	write-host "Succeeded."
	write-host (logTime)"Connecting to $dstInst..." -nonewline
	$dstSrv = new-object ('Microsoft.SqlServer.Management.Smo.Server') $dstInst
	$dstDatabases = $dstSrv.Databases
	write-host "Succeeded."
}
catch [System.Exception]
{
	write-host "Failed."
	$_
}

$sqlcmd = "SELECT DB_NAME(database_id) AS DatabaseName, name AS LogicalFileName, physical_name AS PhysicalFileName FROM sys.master_files AS mf WHERE database_id > 4;"

try
{
	New-PSDrive -PSProvider FileSystem -Root "\\$($srcInst)\f$\Backup" -Name "srcDrive"
	New-PSDrive -PSProvider FileSystem -Root "\\$($dstInst)\e$\Refresh" -Name "dstDrive"
}
catch [System.Exception]
{
	write-host "Mounting PSDrives failed"
	$_
	Remove-PSDrive "srcDrive"
	Remove-PSDrive "dstDrive"
}
		
foreach ($db in $srcDatabases)
{
	if ($db.name -ne "master" -AND $db.name -ne "model" -AND $db.name -ne "msdb" -AND $db.name -ne "tempdb")
	{
		try
		{
			$file = (ls "srcDrive:\$($db.name)" | where { $_.creationtime.day -eq ($(date).day -1) })
			$srcFile = $file.fullname
			$dstFile = "dstDrive:\$($file.name)"
			cp $srcFile $dstFile -verbose
		}
		catch [System.Exception]
		{
			write-host "File copy failed for $($db.name)"
			$_
			Remove-PSDrive "srcDrive"
			Remove-PSDrive "dstDrive"
		}
		try
		{
			$sqlRstCmd = "DROP DATABASE $($db.name); RESTORE DATABASE $($db.name) FROM DISK = 'E:\Refresh\$($file.name)';"
			Invoke-Sqlcmd -ServerInstance $dstInst -Query $sqlRstCmd
		}
		catch [System.Exception]
		{
			write-host "File copy failed for $($db.name)"
			$_
			Remove-PSDrive "srcDrive"
			Remove-PSDrive "dstDrive"
		}
		
	}
}
Remove-PSDrive "srcDrive"
Remove-PSDrive "dstDrive"


