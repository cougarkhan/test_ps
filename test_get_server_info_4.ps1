﻿param([string[]]$computers)


function getServerInfo
{
    param ([string[]]$servers)

    $mb = (1024 * 1024)
    $gb = ((1024 * 1024) * 1024)

    foreach ($server in $servers)
    {
        Invoke-Command -ComputerName $server -UseSSL -ScriptBlock {
            $serverInfo = New-Object System.Object
            #$serverInfo | Add-Member NoteProperty OS_Info -value (
            $osInfo = Get-WmiObject -query "SELECT Caption,Version,FreePhysicalMemory,ServicePackMajorVersion,ServicePackMinorVersion,OSArchitecture FROM WIN32_OperatingSystem" | Select Caption,Version,FreePhysicalMemory,ServicePackMajorVersion,ServicePackMinorVersion,OSArchitecture
            $totalPhysicalMemory = ((Get-WmiObject -query "SELECT TotalPhysicalMemory FROM WIN32_ComputerSystem" | Select -exp TotalPhysicalMemory))
            $totalPhysicalMemory / $mb
            $serverInfo | Add-Member NoteProperty -name TotalMem -value $totalPhysicalMemory
            $freePhysicalMemory = (($osInfo.FreePhysicalMemory)) 
            $serverInfo | Add-Member NoteProperty -name FreeMem -value $freePhysicalMemory
            $serverInfo | Add-Member NoteProperty -name OS_Name -value ($osCaption = $osInfo.Caption)
            $serverInfo | Add-Member NoteProperty -name OS_Version -value ($osVersion = $osInfo.Version)
            $serverInfo | Add-Member NoteProperty -name OS_SPMajVer -value ($osServicePackMaj = $osInfo.ServicePackMajorVersion)
            $serverInfo | Add-Member NoteProperty -name OS_SPMinVer -value ($osServicePackMin = $osInfo.ServicePackMinorVersion)
            $serverInfo | Add-Member NoteProperty -name OS_Arch -value ($osArch = $osInfo.OSArchitecture)
            $hardDrives = Get-WmiObject -query "SELECT DeviceID,VolumeName,Size,FreeSpace FROM WIN32_LogicalDisk WHERE DriveType = 3" | Select DeviceID,VolumeName,Size,FreeSpace
            foreach ($disk in $hardDrives)
            {
                $name = "Disk_$($disk.DeviceID)"
                $serverInfo | Add-Member NoteProperty -name $name -value $disk
                $name
            }
            $networkAdapters = Get-WmiObject -query "SELECT Index,Description,DNSDomain,DNSHostName,IPAddress,IPSubnet,DefaultIPGateway,MACAddress,DNSServerSearchOrder FROM WIN32_NetworkAdapterConfiguration WHERE IPEnabled = True" | Select Index,Description,DNSDomain,DNSHostName,IPAddress,IPSubnet,DefaultIPGateway,MACAddress,DNSServerSearchOrder
            foreach ($nic in $networkAdapters)
            {
                $name = "Nic_$($nic.Index)"
                $serverInfo | Add-Member NoteProperty -name $name -value $nic
                $name
            }
            $serverInfo
        }
    }
    #$serverInfo | export-csv "C:\temp\server_info_$(get-date -f yyyyMMdd-hhmmss).csv"
}

getServerInfo $computers | ft
