set-content -Path "C:\temp\sa_done.txt" -value "Open Uniques file..."
$uniques = gc "C:\temp\20130822_uniques.txt"
add-content -Path "C:\temp\sa_done.txt" -value "Open CSV file..."
$sa_scan = import-csv "C:\temp\sa_scan_20130822.csv"
add-content -Path "C:\temp\sa_done.txt" -value "Done"


workflow find-uniques {
	param ($u, $sa)

	foreach -parallel($item in $u) {
		
		$hits = $sa_scan | where-object -FilterScript {$_.Credential_ID -like $item}
		$count = $hits.count

		add-content -Path "C:\temp\sa_done.txt" -value "$item occurs : $count times.`n"
		
		add-content -Path "C:\temp\sa_done.txt" -value $hits
		
		add-content -Path "C:\temp\sa_done.txt" -value  "`n"
		
	}
}

find-uniques $uniques $sa_scan