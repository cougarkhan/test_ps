$socket = @(5002,5011,5013,5021,5025,5026,5027,5028,5029,5033,5034,5036,5038,5041,5044,5045,5046,5047,5048,5049,5051,
5052,5053,5056,5057,5059,5060,5061,5062,5063,5064,5065,5066,5067,5068,5069,5070,5071,5072,5073,5074,5075,5076,5077,
5078,5079,5080,5082,5083,5084,5085,5086,5087,5088,5089,5090,5091,5093,5094,5095,5096,5098,5099,5100,5101,5111)
$dsxServer = "bis-appdsx-tst.bussops.ubc.ca"
$dopsServer = "bis-appdops-tst.bussops.ubc.ca"
$sqlServer = "bis-sqldsx-tst.bussops.ubc.ca"
$servers = @($dsxServer,$dopsServer,$sqlServer)

foreach ($server in $servers) {
	if ((test-connection $server -count 2 -quiet) -eq $false) {
		write-host "$server is offline.  Exiting..."
		exit
	}
}



$dsx = gwmi win32_process -ComputerName $dsxServer -filter "name='Cs.exe'" | where {$_.getowner().user -eq 'dsx'}

try {
	$result = $dsx.terminate()
	if ($result.ReturnValue -eq 0) {
		write-host "Process terminated (Cs.exe)"
	}
} catch {
	write-host "Nothing to terminate (Cs.exe)"
}

Write-host "Restarting $dsxServer..."
try {
	$result = (gwmi -class win32_process -computername $dsxServer -filter "name='notepad.exe'").terminate()
	if ($result.ReturnValue -eq 0) {
		write-host "Process terminated (Notepad.exe)"
	}
} catch {
	write-host "Nothing to terminate (Notepad.exe)"
}

restart-computer -computername $dsxServer -wait -for PowerShell -timeout 300 -force


$r=0

while (!$dops -AND $r -ne 10) { 
	$dops = gwmi win32_process -ComputerName bis-appdops-tst -filter "name='DBSql.exe'" | where {$_.getowner().user -eq 'dsx'}
	$r++
	$r
	sleep -Seconds 1 
}
$r=0









$dops = gwmi win32_process -ComputerName bis-appdops-tst -filter "name='DBSql.exe'" | where {$_.getowner().user -eq 'dsx'}