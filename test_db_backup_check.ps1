param ([string] $b, [int] $m=10, [int]$d=3)

#############################################################################
#
# Check Parameters
#
#############################################################################
function func_Help() {
    Write-Host "HELP!"
    exit
}

Write-Host "`n"
Write-Host "################################################################################"
Write-Host "#"
Write-Host "# Check DB File backup completion"
Write-Host "#"
Write-Host "################################################################################`n`n"
Write-Host "Using the following Parameters: `n`n"
Write-Host "Backup Location:`t" -nonewline
Write-Host $b -foregroundcolor "GREEN"
Write-Host "File Size Margin:`t" -nonewline
Write-Host $m -foregroundcolor "YELLOW"
Write-Host "Number of Backups:`t" -nonewline
Write-Host $d -foregroundcolor "YELLOW"
Write-Host "`n"
Write-Host "################################################################################`n`n"

if(!$b) {
    Write-Host "Backup Location not set!" -foregroundcolor "RED"
    func_Help
} elseif (!(test-path $b)) {
    Write-host "Backup Location not valid!" -foregroundcolor "RED"
    func_Help
}

function return_Date {
    # Format Date Strings
    $seconds = (get-date).Second
    if ($seconds.length -eq 1) { $seconds = "0$seconds" }

    $minute = (get-date).Minute
    if ($minute.length -eq 1) { $minute = "0$minute" }

    $hour = (get-date).Hour
    if ($hour.length -eq 1) { $hour = "0$hour" }

    $day = (get-date).day.tostring()
    if ($day.length -eq 1) { $day = "0$day" }

    $month = (get-date).month.tostring()
    if ($month.length -eq 1) { $month = "0$month" }

    $year = (get-date).year.tostring()

    $new_Date = "$year$month$day"
    
    return $new_Date
}
##########################################################################
#
#  SQL Backup file checking
#
##########################################################################

$log_date = return_Date
$error_log = @()

$file_Age_Limit = (get-date).AddDays(-$d)

Write-Host "Searching Backup folder $parent_Folder..."
$child_Folders = gci $b | where {$_.PSIsContainer -eq $true}
Write-Host "Found the following backup folders:"
$child_Folders
Write-Host "`n`n"

# Get each folder in the backup folder
foreach ($folder in $child_Folders) {
    
    # If child folder exists then get all files within that folder
    if (test-path $folder.Fullname) {
        $files = gci $folder.FullName
    }
    
    # loop through each file in the child folder
    foreach ($file in $files) {
        
        # if the file is older than the specified number of days then send an error that there are old backups still around
        if ($file.CreationDate -lt $file_Age_Limit) {
            $error_log =+ $file.Name + " is older than " + ($file_Age_Limit - $file.CreationTime).days + " days."
            
        } else {
            if (($file_Age_Limit - $file.CreationTime).days -eq 1) {
                $last_Backup_Size = $file.length
            }
        }
    }
}