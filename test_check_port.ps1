param([string]$target,[int]$port)

function check_port {
	param([string]$target,[int]$port)

	$socket = new-object Net.Sockets.TcpClient
	try
	{
		$socket.Connect($target,$port)
	}
	catch ([System.Exception] $e)
	{
		$e.message
	}
	
	$connected = $socket.Connected
	$socket.Close()
	$socket = $null
	
	if ($connected) {
		return $true
	} else {
		return $false
	}
}

check_port $target $port