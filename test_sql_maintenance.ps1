#####################################################################################
#
# Version 1.01
#
# Written on 12/06/2012
# By Michael Pal
# 
#####################################################################################


. .\functions\format_date.ps1
. .\functions\send_email.ps1
. .\functions\db_functions.ps1
. .\functions\sql_db_functions.ps1


$logDate = format_date
$logTime = format_time

$settings = [xml](gc .\etc\sqlbkpconfig.xml)

$server = $settings.config.sqlserver.instance.name
[array]$databases = $settings.config.sqlserver.databases.name
[array]$options = $settings.config.sqlserver.options


$checkIntegrity = $options.checkIntegrity.enabled
$indexDefrag = $options.indexDefrag.enabled
$updateStatistics = $options.updateStatistics.enabled
if (($backupDatabase = $options.backupDatabase.enabled) -eq 1) {
	$backupLocation = $options.backupDatabase.location
}
if (($moveFiles = $options.movefiles.enabled) -eq 1) {
	$moveFileSource = $options.filelocations.sourcefolder
	$moveFileTarget = $options.filelocations.targetfolder
}
if (($cleanupFiles = $options.cleanupFiles.enabled) -eq 1) {
	$cleanupFileSource = $options.filelocations.sourcefolder
}
$fileAgeLimit = $options.filelimit.age

$log = create_log "C:\temp\$server-$logTime-$logDate.txt"

Start-Transcript -Path $log

Write-Host "`n--------------------------------------------"
Write-Host "Loading SQL Assemblies..."
Write-Host "--------------------------------------------`n"
load_assemblies "Microsoft.SqlServer.SMO"
load_assemblies "Microsoft.SqlServer.SmoExtended"
load_assemblies "Microsoft.SqlServer.ConnectionInfo"
load_assemblies "Microsoft.SqlServer.SqlEnum"

Write-Host (log_time)"`tCreating Sql Server Object " -nonewline
Write-Host "$server..." -nonewline
$timeResult = measure-command {
	$svr = new-object "Microsoft.SqlServer.Management.SMO.Server" $server
}
Write-Host "Completed. $timeResult"

#$sqlList = import-csv .\resources\sqlversions.txt

$sqlVer = $svr.VersionString.SubString(0,4)

Write-host "`nProcessing $server SQL Version"$svr.VersionString"`n"

Write-Host "`n--------------------------------------------"
Write-Host "Skipped Operations..."
Write-Host "--------------------------------------------`n"
if ($cleanupFiles -eq 0) { Write-Host (log_time)"`tSkipping Cleaning up of Database files." }
if ($checkIntegrity -eq 0) { Write-Host (log_time)"`tSkipping Table Ingerity checks." }
if ($indexDefrag -eq 0) { Write-Host (log_time)"`tSkipping Index Deframenting." }
if ($updateStatistics -eq 0) { Write-Host (log_time)"`tSkipping Updating Statistics." }
if ($backupDatabase -eq 0) { Write-Host (log_time)"`tSkipping Backing up of Database." }
if ($moveFiles -eq 0) { Write-Host (log_time)"`tSkipping Move of Database Files." }

[array]$dbs = @()

$databases | %{ 
	$tempDB = new-object "Microsoft.SqlServer.Management.SMO.Database"
	$tempDB = $svr.Databases[$_]
	$dbs += $tempDB
}

if ($cleanupFiles -eq 1) {
	Write-Host "`n--------------------------------------------"
	Write-Host "Performing Cleanup Operations..."
	Write-Host "--------------------------------------------`n"
	foreach ($db in $dbs) {
		$dbName = $db.Name
		$folder = "$cleanupFileSource\$dbName\"
		clean_files $folder $fileAgeLimit
	}
}

if ($checkIntegrity -eq 1) {
	Write-Host "`n--------------------------------------------"
	Write-Host "Performing Integrity Checks..."
	Write-Host "--------------------------------------------`n"
	foreach ($db in $dbs) {
		check_table_integ $db
	}
}

if ($indexDefrag -eq 1) {
	Write-Host "`n--------------------------------------------"
	Write-Host "Performing Index Deframentation..."
	Write-Host "--------------------------------------------`n"
	foreach ($db in $dbs) {
		foreach ($table in $db.tables) {
			maint_index_frag $table $sqlVer
		}
	}
}

if ($updateStatistics -eq 1) {
	Write-Host "`n--------------------------------------------"
	Write-Host "Performing Update of Statistics..."
	Write-Host "--------------------------------------------`n"
	foreach ($db in $dbs) {
		foreach ($table in $db.tables) {
			update_stats $table
		}
	}
}

if ($backupDatabase -eq 1) {
	Write-Host "`n--------------------------------------------"
	Write-Host "Performing Database Backups..."
	Write-Host "--------------------------------------------`n"
	foreach ($db in $dbs) {
		backup_db $svr $db $backupLocation
	}
}

if ($moveFiles -eq 1) {
	Write-Host "`n--------------------------------------------"
	Write-Host "Performing Move of Files to Remote Location..."
	Write-Host "--------------------------------------------`n"
	move_files $moveFileSource $moveFileTarget
}

Stop-Transcript

$serverName = (gci env:COMPUTERNAME).Value
[string]$from = "$serverName@bussops.ubc.ca"
[string]$to = "michael.pal@ubc.ca"
[string]$subject = "$Server Database Maintenance Results"
[string]$body = (gc $log) | out-string
[string]$smtpServer = "mail-relay.ubc.ca"
send-mailmessage -from $from -to $to -subject $subject -body $body -smtpServer $smtpServer