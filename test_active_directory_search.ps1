#$adusers = @()

$strFilter = "(&(objectClass=User)(!objectClass=Computer))"

$objDomain = New-Object System.DirectoryServices.DirectoryEntry

$objSearcher = New-Object System.DirectoryServices.DirectorySearcher
$objSearcher.SearchRoot = $objDomain
$objSearcher.Filter = $strFilter

$colPropertiesList = "cn"

foreach ($prop in $colPropertiesList) { $objSearcher.PropertiesToload.Add($prop) }

$adusers = $objSearcher.FindAll()

foreach ($user in $adusers) { $item = $user.Properties; $item.cn }