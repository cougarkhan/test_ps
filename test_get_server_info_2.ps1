$computers = import-csv C:\temp\servers\windows.csv

$ead = get-credential -Message "Enter Credentials to connect to EAD"
$bussops = get-credential -Message "Enter Credentials to connect to BUSSOPS"
$local = get-credential -Message "Enter Credentials to connect to Local Admin Account"

$os = @()
$max = $computers.count
$counter = 1

foreach ($pc in $computers) {
	[int]$num = ($counter / $max) * 100
	write-progress -activity "Checking..." -currentoperation "$num% $pc" -status "Please Wait." -percentcomplete $num
	switch ($pc.domain) {
		ead.ubc.ca {$os += (gwmi -computername $pc.Name -class win32_operatingsystem -credential $ead | select *); break}
		bussops.ubc.ca {$os += (gwmi -computername $pc.Name -class win32_operatingsystem -credential $bussops | select *); break}
		default {$os += (gwmi -computername $pc.Name -class win32_operatingsystem -credential $local | select *); break}
	}
	
	$counter = $counter + 1
}

$os | ft PSComputername,Caption
