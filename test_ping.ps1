param([string]$machine)

$machine = $machine.Trim()
$ping = new-object System.Net.Networkinformation.Ping
$pingResult = $ping.send("$machine")

if ($pingResult.status -match "Success") {
   Write-Host "$machine is Online" -foregroundcolor "Green"
}
