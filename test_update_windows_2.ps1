$updateSession = new-object -com Microsoft.update.Session
$updateSearcher = $updateSession.CreateUpdateSearcher()
$searchParameters = "IsInstalled=0 and ISHidden=0"
$searchResult = $updateSearcher.search($searchParameters)

if ($searchResult.Updates.Count -le 0) {
	Write-Host "System is Up to Date."
	exit
}

$tableFormat1 = @{Expression={"{0:N2}" -f ($_.MaxDownloadSize / (1024*1024)) + " MB"};Label="Download Size"}, @{Expression={"KB" + $_.KBArticleIDs};Label="KB Article"}, @{Expression={$_.Title};Label="Title";width=25}
				
Write-Host "`n`nThe following updates are available for download...`n"
$searchResult.Updates | FT $tableFormat1 -autosize

[int]$downloadSize = 0
$rebootNeeded = @()

foreach ($item in $searchResult.Updates) {
	$downloadSize += [int]$item.MaxDownloadSize
	$item.RebootRequired
	if ($item.RebootRequired -eq $true) {
		$rebootNeeded += $item.KBArticleIDs
	}
}

$downloadSize = $downloadSize / (1024*1024)

Write-Host "`n`nTotal Download Size is $downloadSize MB`n"
Write-Host $rebootNeeded.Count "of" $searchResult.Updates.Count "updates will require a system restart.`n"
if ($rebootNeeded.Count -gt 0) {
	Write-Host "The following updates will require a system restart..."
}
Write-Host $rebootNeeded -foregroundcolor YELLOW