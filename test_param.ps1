[CmdletBinding()]param([string]$taskName, [string]$userAccount, [string]$scriptPath="P:\Scripting\Powershell\check_is_app_responding.ps1", [string]$computerName="localhost")

set-alias wh write-host

$params = $PSCmdlet.MyInvocation.BoundParameters

if (!$taskName) { $taskName = (read-host "Enter Task Name") }
if (!$userAccount) { $userAccount = (read-host "Enter Account task will run under") }
if (!$scriptPath) { $scriptPath = (read-host "Enter full path to script") }

	 
& schtasks /create /S $computerName /tn $taskName /tr "C:\windows\system32\windowspowershell\v1.0\powershell.exe -file $scriptPath" /sc minute /mo 5 /ru $userAccount /it

wh "Created task " -nonewline
wh $taskName -nonewline -foregroundcolor YELLOW
wh " Successfully." 
wh "This Task will run every " -nonewline
wh "5" -nonewline -foregroundcolor GREEN
wh " minutes under the account " -nonewline
wh "$userAccount.`n`n" -foregroundcolor GREEN

& schtasks /query /S $computerName /TN $taskName /V /FO LIST
