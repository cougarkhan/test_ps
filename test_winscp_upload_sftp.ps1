# Load WinSCP .NET assembly
[Reflection.Assembly]::LoadFile("$PSScriptRoot\WinSCPnet.dll") | Out-Null

 
function FileTransferProgress
{
    Param($e)
 
    # New line for every new file
    if (($script:lastFileName -ne $Null) -and ($script:lastFileName -ne $e.FileName))
    {
        Write-Host
    }
 
    # Print transfer progress
    Write-Host -NoNewline ("`r{0} ({1:P0})" -f $e.FileName, $e.FileProgress)
 
    # Remember a name of the last file reported
    $script:lastFileName = $e.FileName
}
 
# Main script
$script:lastFileName = $Null
 
try
{
    $sessionOptions = New-Object WinSCP.SessionOptions
    $sessionOptions.Protocol = [WinSCP.Protocol]::Sftp
    $sessionOptions.HostName = "bis-fs1-prd.ead.ubc.ca"
    $sessionOptions.UserName = "mpadmin" #Read-Host -Prompt "Username"
    $sessionOptions.Password = Read-Host -Prompt "Password for $($sessionOptions.UserName)"
	$sessionOptions.SshHostKeyFingerprint = "ssh-rsa 2048 0d:07:ad:87:5b:70:db:d5:63:3a:36:9a:d0:d4:2f:64"
 
    $session = New-Object WinSCP.Session
    try
    {
        # Will continuously report progress of transfer
        $session.add_FileTransferProgress( { FileTransferProgress($_) } )
         # Connect
        $session.Open($sessionOptions)
		$to = new-object WinSCP.TransferOptions
		$to.TransferMode = [WinSCP.TransferMode]::Binary
		$transfer = $session.PutFiles("D:\Temp\PME_7.2.2.ISO", "/", $False, $to)
		$transfer.Check()
		
		foreach ($transfer in $tr.Transfers)
		{
			Write-Host ("Upload of {0} succeeded" -f $transfer.FileName)
		}
    }
    finally
    {
        # Terminate line after the last file (if any)
        if ($script:lastFileName -ne $Null)
        {
            Write-Host "Hello"
        }
 
        # Disconnect, clean up
        $session.Dispose()
    }
 
    exit 0
}
catch [Exception]
{
    Write-Host $_.Exception
    exit 1
}