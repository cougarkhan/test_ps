. .\functions\format_date.ps1
. .\functions\send_email.ps1
. .\functions\db_functions.ps1
. .\functions\orcl_db_functions.ps1

$logDate = format_date
$logTime = format_time

$settings = [xml](gc .\etc\orclbkpconfig.xml)

$server = $settings.oracle.server
[array]$databases = $settings.oracle.databases
[array]$options = $settings.oracle.options

if (($backupDatabase = $options.backupDatabase.enabled) -eq 1) {
	$backupLocation = $options.backupDatabase.location
}
if (($moveFiles = $options.movefiles.enabled) -eq 1) {
	$moveFileSource = $options.filelocations.sourcefolder
	$moveFileTarget = $options.filelocations.targetfolder
}
if (($cleanupFiles = $options.cleanupFiles.enabled) -eq 1) {
	$cleanupFileSource = $options.filelocations.sourcefolder
}

$fileAgeLimit = $options.filelimit.age

$log = create_log "C:\temp\$server-$logTime-$logDate.txt"

Start-Transcript -Path $log

Write-Host "`n--------------------------------------------"
Write-Host "Skipped Operations..."
Write-Host "--------------------------------------------`n"

