$ion_list = import-csv "P:\Projects\ION Server Migration\ion.csv"
$nmc_list = import-csv "P:\Projects\ION Server Migration\nmc.csv"
$meters_list = import-csv "P:\Projects\ION Server Migration\meters.csv"
$meters_mac_list= import-csv "P:\Projects\ION Server Migration\meters_mac.csv"

$sqlServer = "localhost\local"
$sqlDatabase = "ubc_prod"

$sqlConn = New-Object System.Data.SqlClient.SqlConnection
$sqlConn.ConnectionString = "Server = $sqlServer; Database = $sqlDatabase; Integrated Security = True"

$sqlCmd = New-Object System.Data.SqlClient.SqlCommand

$sqlCmd.Connection = $sqlConn

$result = @()

foreach ($ion in $ion_list)
{
	$id = $ion.bl_id
	$meter_item = $NULL
	$mac = $NULL
	$sqlQuery1 = "
		SELECT 
			[afm].[bl].bl_id as Building_ID, 
			[afm].[bl].option1 as Group_ID,
			[afm].[bl].name as Name,
			[afm].[bl_shortnames].short_name as Short_Name
		FROM
			[afm].[bl]
		INNER JOIN 
			[afm].[bl_shortnames]
		ON
			[afm].[bl].bl_id = [afm].[bl_shortnames].bl_id
		WHERE
			[afm].[bl].bl_id = '$id';"
	add-content -value $sqlQuery1 -path "P:\Projects\ION Server Migration\query.txt"
}