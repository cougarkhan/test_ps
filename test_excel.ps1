$ping = new-object System.Net.NetworkInformation.Ping

$excel = new-object -com Excel.Application
$excel.visible = $False
$excel.displayalerts=$False

$wbk = $excel.Workbooks.open("C:\temp\LAND02_BMSUTILNET_IP_Addresses.xlsx")

$wbk.Worksheets.item(2).Copy($wbk.Worksheets.item(3))
$wbk.Save()
$wbk.close()

$wks = $wbk.WorkSheets.Item("Sheet1")


[string]$ip_Start = $wks.Cells.Item(2,2).Value()
[string]$ip_End = $wks.Cells.Item(3,2).Value()

$ip_Range_Start = $ip_Start.Split(".")
$ip_Range_End = $ip_End.Split(".")

[int]$ip_Column_Start_Cell = 14
$ip_Column_Current_Cell = $ip_Column_Start_Cell
$ip_Column_End_Cell = (([int]$ip_Range_End[3] - [int]$ip_Range_Start[3]) + 15)

while ($wks.Cells.Item($ip_Column_Current_Cell,1) -AND $ip_Column_Current_Cell -le $ip_Column_End_Cell) {
    $host_IP = $wks.Cells.Item($ip_Column_Current_Cell,1).Value()
    $Reply = $ping.send($host_IP)
    
    if ($Reply.Status -eq "Success") {
        if ($host_ComputerSystem = Get-WmiObject Win32_ComputerSystem -computername $host_IP) {
            $wks.Cells.Item($ip_Column_Current_Cell,2).Value() = $host_ComputerSystem.Description
            $wks.Cells.Item($ip_Column_Current_Cell,3).Value() = $host_ComputerSystem.Name
        }
    }
    $host_IP    
    $ip_Column_Current_Cell++
}

write-host OUT!

$wbk.SaveAs("C:\temp\Book1.xlsx")
$wbk.Close()
$excel.Quit()
[System.Runtime.Interopservices.Marshal]::ReleaseComObject($excel)
Remove-Variable excel
[System.GC]::Collect()
Get-Process excel | Stop-Process


































#$xl.Cells.Item(1,1).Value()