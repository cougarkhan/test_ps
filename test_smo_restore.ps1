﻿param ([string]$ns, [string]$nd, [string]$s, [string]$t, [string]$l="C:\temp\smolog.log")

#############################################################################
# Load SQL Server assemblies
#############################################################################

function load_assemblies {
	param([string]$name)
	$startTime = get-date
	
	Write-Host "Loading .NET SQL Assembly " -nonewline
	Write-Host "$name" -nonewline -foregroundcolor "CYAN"
	Write-host " ... " -nonewline
	if ([System.Reflection.Assembly]::LoadWithPartialName($name)) {
		$endTime = get-date
		Write-Host "Completed." -foregroundcolor "GREEN" -nonewline
		$compTime = ($endTime - $startTime).TotalSeconds
		Write-Host " ($compTime secs)"
	} else {
		$endTime = get-date
		Write-Host "Failed" -foregroundcolor "RED" -nonewline
		$compTime = ($endTime - $startTime).TotalSeconds
		Write-Host " ($compTime secs)"
	}
}

load_assemblies "Microsoft.SqlServer.SMO"
load_assemblies "Microsoft.SqlServer.SmoExtended"
load_assemblies "Microsoft.SqlServer.ConnectionInfo"
load_assemblies "Microsoft.SqlServer.SmoEnum"

function check_db_existence {
	param($sqlServerName)
	
	if ($sqlServerName.Databases[$databaseName]) {
		Write-Host "$databaseName " -nonewline -foregroundcolor "CYAN"
		Write-Host "does not exist."
	} else {
		Write-Host "$databaseName " -nonewline -foregroundcolor "CYAN"
		Write-Host "does exist."
	}
}

function write_log {
	param ([string]$logPath, [string] $entry, [bool]$append=$true)
	
	if (!(Test-Path $logPath)) {
		Write-Host "`nCreating log at " -nonewline
		Write-Host "$logPath" -nonewline -foregroundcolor "CYAN"
		Write-Host " ... " -nonewline
		
		if ($log = New-Item -Path $logPath -type File -Force) {
			Write-Host "Success." -foregroundcolor "GREEN"
		} else {
			Write-Host "Failure." -foregroundcolor "RED"
		}
	} else {
		Write-Host "Retrieving log file " -nonewline
		Write-Host "$logPath" -nonewline -foregroundcolor "CYAN"
		Write-Host " ... " -nonewline
		
		if ($log = (gi $logPath)) {
			Write-Host "Success." -foregroundcolor "GREEN"
		} else {
			Write-Host "Failure." -foregroundcolor "RED"
		}
	}
	
	if ($append -eq $true) {
		$entry | Out-File -FilePath $log -Append
	} else {
		$entry | Out-File -FilePath $log
	}
	
}

function backup_db {
	param ([string]$server, [string]$database, [string]$backupDir)
	
	$startTime = get-date
	
	Write-Host "Creating Sql Server Object " -nonewline
	Write-Host "$server " -nonewline -foregroundcolor "CYAN"
	Write-Host " ... " -nonewline
	$sqlServer = New-Object ("Microsoft.SqlServer.Management.Smo.Server") $server
	Write-Host "Completed." -foregroundcolor "GREEN"
		
	$timestamp = Get-Date -Format yyyyMMddHHmmss
	
	$db = $sqlServer.Databases[$database]
	$dbName = $db.Name
	
	$backupFile = $backupDir + "\" + $dbName + "_" + $timestamp + ".bak"
		
	Write-Host "Creating Sql Server Backup Object " -nonewline
	Write-Host " ... " -nonewline	
	$smoBackup = New-Object ("Microsoft.SqlServer.Management.Smo.Backup")
	Write-Host "Completed." -foregroundcolor "GREEN"
	
	
	$smoBackup.Action = "Database"
	$smoBackup.BackupSetDescription = "Full Backup of " + $dbName
	$smoBackup.BackupSetName = $dbName + " Backup"
	$smoBackup.Database = $dbName
	$smoBackup.MediaDescription = "Disk"
	$smoBackup.Devices.AddDevice($backupDir + "\" + $dbName + "_" + $timestamp + ".bak", "File")
	Write-Host "Backing up Database " -nonewline
	Write-Host "$database " -nonewline -foregroundcolor "CYAN"
	Write-Host "to " -nonewline
	Write-Host "$backupFile " -nonewline -foregroundcolor "CYAN"
	Write-Host " ... " -nonewline
	$smoBackup.SqlBackup($sqlServer)
	Write-Host "Completed." -foregroundcolor "GREEN"

	$logOut = "Backup Dir: " + $backupDir + "\" + $dbName + "_" + $timestamp + ".bak"
	
}

function drop_db {
	param ([string]$server, [string]$database)
	
	$sqlServer = New-Object ("Microsoft.SqlServer.Management.Smo.Server") $server
	$db = $sqlServer.Databases[$database]
	
	$db.DatabaseOptions.UserAccess = "Single"
	
	Write-Host "Kill all open connections to " -nonewline
	Write-Host "$server" -nonewline -foregroundcolor "CYAN"
	Write-Host " ... " -nonewline
	$sqlServer.KillAllProcesses($database)
	if ($sqlServer.EnumProcesses() | where { $_.Database -match $database}) {
		Write-Host "Failure." -foregroundcolor "GREEN"
	} else {
		Write-Host "Success." -foregroundcolor "RED"
	}
		
	Write-Host "Dropping Database " -nonewline
	Write-Host "$database" -nonewline -foregroundcolor "CYAN"
	Write-Host " ... " -nonewline
	$db.drop()

}

function restore_db {
	param ([string]$server, [string]$backupFile)
			
	$sqlServer = New-Object ("Microsoft.SqlServer.Management.Smo.Server") $server
	$backupDevice = New-Object("Microsoft.SqlServer.Management.Smo.BackupDeviceItem") ($backupFile, "File")
	$smoRestore = New-object("Microsoft.SqlServer.Management.Smo.Restore")
	$smoRestoreFile = New-Object("Microsoft.SqlServer.Management.Smo.RelocateFile")
	$smoRestoreLog = New-Object("Microsoft.SqlServer.Management.Smo.RelocateFile")
	
	$smoRestore.NoRecovery = $false;
	$smoRestore.ReplaceDatabase = $true;
	$smoRestore.Action = "Database"
	$smoRestorePercentCompleteNotification = 10;
	$smoRestore.FileNumber = 1
	$smoRestore.Devices.Add($backupDevice)
	
	$smoRestoreDetails = $smoRestore.ReadBackupHeader($sqlServer)
	
	$databaseName = $smoRestoreDetails.Rows[0]["DatabaseName"]
	"Database Name from Backup Header : " + $databaseName
	 
	$smoRestore.Database = $databaseName
	 
	$smoRestoreFile.LogicalFileName = $databaseName + "_Data"
	$smoRestoreFile.PhysicalFileName = $sqlServer.Information.MasterDBPath + "\" + $smoRestore.Database + ".MDF"
	$smoRestoreLog.LogicalFileName = $databaseName + "_Log"
	$smoRestoreLog.PhysicalFileName = $sqlServer.Information.MasterDBLogPath + "\" + $smoRestore.Database + "_1.LDF"
	$smoRestore.RelocateFiles.Add($smoRestoreFile)
	$smoRestore.RelocateFiles.Add($smoRestoreLog)
	 
	drop_db $server $databaseName
	
	check_db_existence $sqlServer
	$smoRestore.SqlRestore($sqlServer)
	check_db_existence $sqlServer
}

function move_files {
	param ([string]$source, [string]$target)

	gci $target | foreach {
		Remove-Item $_.FullName -Force
		Write-Host "Removed " -nonewline
		Write-Host $_.FullName -foregroundcolor "CYAN"
	}
	
	gci $source | foreach {
		Move-Item $_.FullName ($target + "\$_") -Force
		Write-Host "Moving " -nonewline
		Write-Host "$_.FullName " -nonewline -foregroundcolor "CYAN"
		Write-Host "to " -nonewline
		Write-Host "$target\$_" -foregroundcolor "CYAN"
	}
}
[array]$dsxDbs = ("AcsData","AcsLog","DSX-SystemSettings","DSX-Update")

$dsxDbs | foreach { backup_db $ns $_ "E:\Refresh" }
move_files $s $t
gci $t | foreach { restore_db $nd $_.FullName }
