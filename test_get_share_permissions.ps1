[cmdletbinding()] 
 
param([Parameter(ValueFromPipeline=$True, ValueFromPipelineByPropertyName=$True)]$Computer = '.')

#$shares = gwmi -Class win32_share -ComputerName $computer | where {!$_.path.contains("user")} | select -ExpandProperty Name 
$shares = gwmi -Class win32_share -ComputerName $computer | select Name,Path

foreach ($share in $shares) {  
    $acl = $null  
    Write-Host "$($share.name) | $($share.path)" -ForegroundColor Green  
	$l = $share.name.Length + $share.path.length
	
    Write-Host $('-' * ($l)) -ForegroundColor Green  
    $objShareSec = Get-WMIObject -Class Win32_LogicalShareSecuritySetting -Filter "name='$($Share.name)'"  -ComputerName $computer 
    try {  
        $SD = $objShareSec.GetSecurityDescriptor().Descriptor    
        foreach($ace in $SD.DACL){   
            $UserName = $ace.Trustee.Name      
            If ($ace.Trustee.Domain -ne $Null) {$UserName = "$($ace.Trustee.Domain)\$UserName"}    
            If ($ace.Trustee.Name -eq $Null) {$UserName = $ace.Trustee.SIDString }      
            [Array]$ACL += New-Object Security.AccessControl.FileSystemAccessRule($UserName, $ace.AccessMask, $ace.AceType)  
            } #end foreach ACE            
        } # end try  
    catch  
        { Write-Host "Unable to obtain permissions for $share" }  
    $ACL  
    Write-Host $('=' * 50)  
    }