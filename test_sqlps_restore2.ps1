$rsfile1 = new-object Microsoft.SqlServer.Management.Smo.RelocateFile
$rsfile1.LogicalFileName = "HistorianDB"
$rsfile1.PhysicalFileName = "E:\MSSQL\MSSQL11.MSSQLSERVER\MSSQL\DATA\HistorianDB.MDF"
$rsfile2 = new-object Microsoft.SqlServer.Management.Smo.RelocateFile
$rsfile2.LogicalFileName = "HistorianDB_log"
$rsfile2.PhysicalFileName = "E:\MSSQL\MSSQL11.MSSQLSERVER\MSSQL\DATA\HistorianDB.LDF"
$rfl = @()
$rfl += $rsfile1
$rfl += $rsfile2
$rfl

Restore-SqlDatabase -ServerInstance . -Database HistorianDB -BackupFile C:\temp\db\HistorianDB_db_20140813183210.bak -ReplaceDatabase -RelocateFile $rfl