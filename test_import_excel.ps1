# import_excel.ps1
# Retrieves data from a mult-worksheet Excel file
# and writes to the console in csv format.

$strExcelFile = "A:\Temp\DSX\L76.CSV"

$objExcel = New-Object -comobject Excel.Application
$objWorkbook = $objExcel.Workbooks.Open($strExcelFile)

write-host "Numer of worksheets: " $objWorkbook.Sheets.Count

$row = 1
$col = 1
$sheet = 1

while ($sheet -le $objWorkbook.Sheets.Count)
{
  $objSheet = $objWorkbook.Sheets.Item($sheet)
  write-Host "Worksheet: " $objSheet.Name

  #Count number of columns in row 1 until first blank
  $numCols = 0
  while ($objSheet.Cells.Item(1,$col).Text -ne "")
  { $col += 1 ; $numCols += 1 }
  $col = 1

  #Iterate through rows, stopping at the first blank in col 1
  while ($objSheet.Cells.Item($row,1).Text -ne "")
  {
    $strLine = ""
    while ($col -le $numCols)
      {
         if ($col -eq 1) {$strLine = $objSheet.Cells.Item($row,$col).Text}
         else {$strLine = $strLine + "," + $objSheet.Cells.Item($row,$col).Text}
         $col += 1
      }
    $strLine >> A:\Temp\DSX\final.csv
    write-host $strLine
    $row += 1
    $col = 1
  }
 
  $sheet += 1
  $row = 1
  $col = 1
  write-host
}