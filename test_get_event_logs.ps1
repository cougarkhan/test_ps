param ([string[]]$servers=@((gci env:COMPUTERNAME).value),[switch]$ad)

$log_date = date -format yyyyMMdd
start-transcript "C:\temp\server_event_logs-$log_date.txt"
write-host "`n`n"

import-module ActiveDirectory

if ($ad) {
	$servers = Get-ADComputer -filter {comment -eq "Server-Windows" -AND info -eq "domain"} | select -exp DNSHostName
}
$event_array = @()
$log_array = @()

$day = ((date).AddDays(-2)).day
$month = ((date).AddDays(-2)).month
$format = "{0,-20} {1,-40} {2,-15} {3,-12} {4,-12}"

$format -f "Time","Server","Log Name","Warnings","Errors"

$i = 1
$count = $servers.count

foreach ($server in $servers) {
		if (test-connection $server -quiet) {
		$application_log_errors = @()
		$application_log_warnings = @()
		$system_log_errors = @()
		$system_log_warnings = @()
		
		write-progress -activity "Gathering Event Logs..." -status "Processing $server.   $i/$count" -percentcomplete ($i / $servers.count * 100)
		
		$time_result = measure-command -expression { 
			$application_log = get-eventlog -logname Application -newest 1000 -computername $server
			$application_log | %{ 
				if ($_.timegenerated.day -gt $day -AND $_.timegenerated.month -eq $month){
					if ($_.entrytype -match "Error") {
						$application_log_errors += $_
					}
					if ($_.entrytype -match "Warning") {
						$application_log_warnings += $_
					}
				} 
			}
			$log_array += $application_log_errors
			$log_array += $application_log_warnings
		}
		if ($application_log_errors.count -gt 0) {
			write-host ($format -f $time_result,$server,"Application",$application_log_warnings.count,$application_log_errors.count) -foregroundcolor RED
		} elseif ($application_log_warnings.count -gt 0) {
			write-host ($format -f $time_result,$server,"Application",$application_log_warnings.count,$application_log_errors.count) -foregroundcolor YELLOW
		} else {
			write-host ($format -f $time_result,$server,"Application",$application_log_warnings.count,$application_log_errors.count) -foregroundcolor GREEN
		}
		

		$time_result = measure-command -expression { 
			$system_log = get-eventlog -logname System -newest 1000 -computername $server
			$system_log | %{ 
				if ($_.timegenerated.day -gt $day -AND $_.timegenerated.month -eq $month){
					if ($_.entrytype -match "Error") {
						$system_log_errors += $_
					}
					if ($_.entrytype -match "Warning") {
						$system_log_warnings += $_
					}
				} 
			}
			$log_array += $system_log_errors
			$log_array += $system_log_warnings
		}
		
		if ($system_log_errors.count -gt 0) {
			write-host ($format -f $time_result,$server,"System",$system_log_warnings.count,$system_log_errors.count) -foregroundcolor RED
		} elseif ($system_log_warnings.count -gt 0) {
			write-host ($format -f $time_result,$server,"System",$system_log_warnings.count,$system_log_errors.count) -foregroundcolor YELLOW
		} else {
			write-host ($format -f $time_result,$server,"System",$system_log_warnings.count,$system_log_errors.count) -foregroundcolor GREEN
		}
		
		# foreach ($log in $log_array) {
			# if ($log.count -gt 0) {
				# foreach ($event in $log) {
					# if ($event.timegenerated -gt ((date).adddays(-1))) {
						
						# $object = new-object system.object
						
						# $object | add-member -type noteproperty -name "Index" -value $event.index
						# $object | add-member -type noteproperty -name "EventID" -value $event.eventid
						# $object | add-member -type noteproperty -name "EntryType" -value $event.entrytype
						# $object | add-member -type noteproperty -name "Time" -value $event.timegenerated
						# $object | add-member -type noteproperty -name "Source" -value $event.source
						# $object | add-member -type noteproperty -name "Username" -value $event.username
						# $object | add-member -type noteproperty -name "Message" -value $event.message
						
						# $event_array += $object
						
						# $object = $null
					# }
				# }
				# $event_array
				# $event_array = $null
			# }
		# }
		
		$i++
	} else {
		Write-host "$server is OFFLINE." -foregroundcolor RED
	}
}
write-host "`n`n"
stop-transcript