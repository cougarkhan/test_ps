$ion_list = import-csv "P:\Projects\ION Server Migration\ion.csv"
$nmc_list = import-csv "P:\Projects\ION Server Migration\nmc.csv"
$meters_list = import-csv "P:\Projects\ION Server Migration\meters.csv"
$meters_mac_list= import-csv "P:\Projects\ION Server Migration\meters_mac.csv"

$sqlServer = "localhost\local"
$sqlDatabase = "ubc_prod"

$sqlConn = New-Object System.Data.SqlClient.SqlConnection
$sqlConn.ConnectionString = "Server = $sqlServer; Database = $sqlDatabase; Integrated Security = True"

$sqlCmd = New-Object System.Data.SqlClient.SqlCommand

$sqlCmd.Connection = $sqlConn

$result = @()

foreach ($meter in $meters_list)
{
	$id = $meter.bl_id
	$ion_item = $NULL
	$mac = $NULL
	$sqlQuery1 = "
		SELECT 
			[afm].[bl].bl_id as Building_ID, 
			[afm].[bl].option1 as Group_ID,
			[afm].[bl].name as Name,
			[afm].[bl_shortnames].short_name as Short_Name
		FROM
			[afm].[bl]
		INNER JOIN 
			[afm].[bl_shortnames]
		ON
			[afm].[bl].bl_id = [afm].[bl_shortnames].bl_id
		WHERE
			[afm].[bl].bl_id = '$id';"
	$sqlCmd.CommandText = $sqlQuery1
	$sqlAdapter = New-Object System.Data.SqlClient.SqlDataAdapter
	$sqlAdapter.SelectCommand = $sqlCmd
	$dataSet = New-Object System.Data.DataSet
	$sqlAdapter.Fill($dataSet,"Buildings") | Out-Null
	
	foreach ($ion in $ion_list)
	{
		if ($id -eq $ion.bl_id)
		{
			Write-Host "$id = $($ion.bl_id) $($ion.Name)"
			$ion_item = $ion
		}
	}
	
	foreach ($meter_mac in $meters_mac_list)
	{
		if ($meter.cct -eq $meter_mac.cct)
		{
			$mac = $meter_mac.mac
		}
	}
	
	foreach ($building in $dataSet.Tables["Buildings"])
	{
		$hash = @{
			Archibus_Building_ID = $building.Building_ID
			Archibus_Group_ID = $building.Group_ID
			Archibus_Name = $building.Name
			Archibus_Short_Name = $building.Short_Name
			Transmogrifier_Building_ID = $meter.bl_id
			Transmogrifier_Name = $meter.name
			Transmogrifier_CCT_Port = $meter.cct
			Transmogrifier_Room_Number = $meter.room
			Transmogrifier_Switch_Port = $meter.port
			Transmogrifier_MAC_Address = $mac
			Transmogrifier_VLAN_Name = $meter.vlan
			Transmogrifier_Port_Type = $meter.type
			ION_Building_ID = $ion_item.bl_id
			ION_Name = $ion_item.name
			ION_Type = $ion_item.type
			ION_Address = $ion_item.address
			ION_Description = $ion_item.description
		}
		$result += New-Object PSObject -Property $hash
	}
	$sqlAdapter.Dispose()
	#$dataSet.Clear()
}
#$result | export-csv C:\temp\ion\meters.csv

$result | select Archibus_Name, 
	Archibus_Building_ID, 
	Archibus_Group_ID, 
	Archibus_Short_Name, 
	ION_Building_ID, 
	ION_Name, 
	ION_Type, 
	ION_Address, 
	ION_Description, 
	Transmogrifier_Building_ID, 
	Transmogrifier_CCT_Port, 
	Transmogrifier_Room_Number, 
	Transmogrifier_Switch_Port, 
	Transmogrifier_MAC_Address, 
	Transmogrifier_VLAN_Name, 
	Transmogrifier_Port_Type | export-csv "P:\Projects\ION Server Migration\complete.csv" -Encoding ASCII
	