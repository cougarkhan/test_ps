function checkid {

	param ($user = (get-credential))

	$domain = "EAD"
	$cndomain = "DC=EAD,DC=UBC,DC=CA"

	$ct = [System.DirectoryServices.AccountManagement.ContextType]::Domain
	$pc = New-Object System.DirectoryServices.AccountManagement.PrincipalContext($ct, $domain, $cndomain)

	
	while ($pc.ValidateCredentials($user.username,$user.GetNetworkCredential().password) -eq $false)
	{
		$user = (get-credential)
	}
}


