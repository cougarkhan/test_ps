param ([string]$file)

$jobs = @()

if (test-path $file) {
    $list = gc $file
    
    foreach ($thing in $list) {
        $jobs += start-job -filepath "P:\Scripting\Powershell\test_ping.ps1" -ArgumentList $thing
    }
} else { Write-host "File not found!" -foregroundcolor "Red" }

Start-sleep 5

foreach ($job in $jobs) {
    receive-job -id $job.id -keep
}