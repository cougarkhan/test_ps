cp "C:\temp\LAND02_BMSUTILNET_IP_Addresses - Copy.xlsx" "C:\temp\LAND02_BMSUTILNET_IP_Addresses.xlsx" -force
$excel = new-object -com Excel.Application
$excel.visible = $True
$excel.displayalerts=$False
$excel_pid = (Get-Process -Name excel)| Where-Object {$_.MainWindowHandle -eq $excel.HWND}

$wbk = $excel.Workbooks.open("C:\temp\LAND02_BMSUTILNET_IP_Addresses.xlsx")
$wks_main = $wbk.Worksheets.item(1)

$counter = 2
#do
while (($wks_main.Cells.Item($counter,1).Value()) -ne $null)
{
	try {
		if ($wbk.Worksheets.item($wks_main.Cells.Item($counter,1).Value()))
		{
			$sheet_name = $wks_main.Cells.Item($counter,1).Value()
			write-host "$sheet_name exists." -foregroundcolor GREEN
		}
	}
	catch {
		$sheet_name = $wks_main.Cells.Item($counter,1).Value()
		write-host "Creating $sheet_name..." -foregroundcolor YELLOW
		
		$last_item = ($wbk.Worksheets.Count)
		$wbk.Worksheets.item($last_item).Copy($wbk.Worksheets.item($last_item))
		$last_item = ($wbk.Worksheets.Count)
		$new_item = $last_item - 1

		$name = $wks_main.Cells.Item($counter,1).Value()
		$description = $wks_main.Cells.Item($counter,2).Value()
		$fwsm = $wks_main.Cells.Item($counter,3).Value()
		$network = $wks_main.Cells.Item($counter,4).Value()
		$mask = $wks_main.Cells.Item($counter,5).Value()
		$gateway = $wks_main.Cells.Item($counter,6).Value()
		$range = $wks_main.Cells.Item($counter,7).Value()
		$vrf = $wks_main.Cells.Item($counter,8).Value()
		$dns = $wks_main.Cells.Item($counter,9).Value()
		$vlan_id = $wks_main.Cells.Item($counter,10).Value()

		$hosts = ($mask.Split('.') | %{ 255 - $_ })[3]

		$wbk.Worksheets.item($new_item).name = $name

		$wks = $wbk.Worksheets.item($new_item)

		$wks.Cells.Item(2,2).Value() = $fwsm
		$wks.Cells.Item(3,2).Value() = $vlan_id
		$wks.Cells.Item(4,2).Value() = $vrf
		$wks.Cells.Item(5,2).Value() = $network.Trim()
		$wks.Cells.Item(6,2).Value() = $mask
		$wks.Cells.Item(7,2).Value() = $gateway
		$wks.Cells.Item(8,2).Value() = $range
		
		$cell_row = 12
		
		#$ip = $network.split("/")
		#$x = $ip[0].split("."); 
		$x = $network.split("."); 
		$new_ip = "$($x[0]).$($x[1]).$($x[2])."
		
		foreach ($row in $cell_row..$($cell_row + 254)) 
		{ 
			$value = ($row-$cell_row)+1
			$wks.Cells.item($row,1).Value() = "$new_ip$value"
			switch ($value) 
			{
				252 { $wks.Cells.item($row,2).Value() = "Reserved for HSRP Routing" }
				253 { $wks.Cells.item($row,2).Value() = "Reserved for HSRP Routing" }
				254 { $wks.Cells.item($row,2).Value() = "Gateway" }
				255 { $wks.Cells.item($row,2).Value() = "Broadcast" }
				
			}
		}
		
		$wks_main.Hyperlinks.Add($wks_main.Cells.Item($counter,1),"","'$($wks_main.Cells.Item($counter,1).Value())'!A1","",$wks_main.Cells.Item($counter,1).Value()) | out-null
		$wks.Hyperlinks.Add($wks.Cells.Item(2,4),"","$($wks_main.Name)!A1","","Network Overview") | out-null
		
	}
	$counter++
	
}

$wbk.Save()
$wbk.close()

#(ps -d $excel_pid.id).kill()
