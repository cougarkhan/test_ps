#**************************************************************************************
# Format Date Strings
#**************************************************************************************
$seconds = (get-date).Second
if ($seconds.length -eq 1) { $seconds = "0$seconds" }

$minute = (get-date).Minute
if ($minute.length -eq 1) { $minute = "0$minute" }

$hour = (get-date).Hour
if ($hour.length -eq 1) { $hour = "0$hour" }

$day = (get-date).day.tostring()
if ($day.length -eq 1) { $day = "0$day" }

$month = (get-date).month.tostring()
if ($month.length -eq 1) { $month = "0$month" }

$year = (get-date).year.tostring()

$backupDate = "$year$month$day"

&"C:\Program Files\7-zip\7z.exe" a A:\Temp\trs-temp\TRS-$backupDate.7z A:\Temp\TRS