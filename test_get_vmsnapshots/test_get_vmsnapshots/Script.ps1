﻿function Get-InstallPath {
   $regKeys = Get-ItemProperty "hklm:\software\VMware, Inc.\VMware vSphere PowerCLI" -ErrorAction SilentlyContinue
   
   #64bit os fix
   if($regKeys -eq $null){
      $regKeys = Get-ItemProperty "hklm:\software\wow6432node\VMware, Inc.\VMware vSphere PowerCLI"  -ErrorAction SilentlyContinue
   }

   return $regKeys.InstallPath
}
function LoadSnapins(){
   $snapinList = @( "VMware.VimAutomation.Core", "VMware.VimAutomation.Vds", "VMware.VimAutomation.License", "VMware.DeployAutomation", "VMware.ImageBuilder", "VMware.VimAutomation.Cloud")

   $loaded = Get-PSSnapin -Name $snapinList -ErrorAction SilentlyContinue | % {$_.Name}
   $registered = Get-PSSnapin -Name $snapinList -Registered -ErrorAction SilentlyContinue  | % {$_.Name}
   $notLoaded = $registered | ? {$loaded -notcontains $_}
   
   foreach ($snapin in $registered) {
      if ($loaded -notcontains $snapin) {
         Add-PSSnapin $snapin
      }

      # Load the Intitialize-<snapin_name_with_underscores>.ps1 file
      # File lookup is based on install path instead of script folder because the PowerCLI
      # shortuts load this script through dot-sourcing and script path is not available.
      $filePath = "{0}Scripts\Initialize-{1}.ps1" -f (Get-InstallPath), $snapin.ToString().Replace(".", "_")
      if (Test-Path $filePath) {
         & $filePath
      }
   }
}
LoadSnapins


$vcenters = @{}
$vcenters.Add("klinck","vcenter.vss.it.ubc.ca")
$vcenters.Add("udc","vss-vcsrvp01.ead.ubc.ca")
$vcenters.Add("devl","vss-vcsrvd01.ead.ubc.ca")
$vcenters.Add("lfsc","vss-vcsp31.ead.ubc.ca")
$vcenters.Add("unknown","vss-vcsp01.ead.ubc.ca")

function get-vmsnapshots {
    get-vm | Get-Snapshot | select Created,VM,SizeGB,Name,Description
}

$smtp_server = "smtp.interchange.ubc.ca"
$smtp = New-Object Net.Mail.SmtpClient($smtp_server)
$smtp_from = "michael.pal@ubc.ca"
$smtp_to = "michael.pal@ubc.ca"
$subject = "VM Snapshots"

$style = "<style>"
$style = $style + "TH{padding-left: 10px;text-align: left;}"
$style = $style + "TD{padding-left: 10px;}"
$style = $style + "</style>"

$message = New-Object System.Net.Mail.MailMessage $smtp_from, $smtp_to
$message.Subject = $subject
$message.IsBodyHTML = $true

$body += "<html><head>$style</head><body>"
foreach ($vcenter in $vcenters.keys) {
	
	$body += "<h1>$($vcenters.item($vcenter))</h1>"
	
	$conn = connect-viserver $vcenters.item($vcenter)
	$snapshots = get-vmsnapshots
	
	foreach ($snapshot in $snapshots) { 
		$snapshot.SizeGB = [int]$snapshot.SizeGB
	}
	
	$body += $snapshots | convertto-html -fragment
}

$body += "</body></html>"
$message.Body = $body

$smtp.Send($message)
