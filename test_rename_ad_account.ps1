###############################################################################################################
## Rename Local Admin account to bopsis and set password
###############################################################################################################

param ([string]$machine, [string]$username)

$user = [adsi]("WinNT://" + $machine + "/" + $username + ",user")
$user.psbase.rename("tempuser3")
$user.psbase.invoke("SetPassword", "good2go")

###############################################################################################################
## Add computer to domain
###############################################################################################################

add-computer -domainname "bussops.ubc.ca" -confirm -whatif

# Get file count excluding directories.
Write-Host (get-childitem \\bops-file1\bisinstallers$\i386\winntupg -recurse | where {!($_.PSISContainer)}).Count

