$ServerName = "bis-dc1"
$ContainerName = "bussops.ubc.ca"

$Scope = New-Object Management.ManagementScope("\\$ServerName\root\MicrosoftDNS")
$Path = New-Object Management.ManagementPath("MicrosoftDNS_Zone")
$Options = New-Object Management.ObjectGetOptions($Null, [System.TimeSpan]::MaxValue, $True)
 
$ZoneClass = New-Object Management.ManagementClass($Scope, $Path, $Options)

$Zones = Get-WMIObject -Computer $ServerName -Namespace "root\MicrosoftDNS" -Class "MicrosoftDNS_Zone"

