#function a {
#	param ([string]$file)
	write-host "Processing $file ..."
	
	Write-host "Loading Libraries...." -nonewline
	[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.SMO") | Out-Null
	[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.SmoExtended") | Out-Null
	[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.ConnectionInfo") | Out-Null
	[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.SmoEnum") | Out-Null
	Write-host "Completed"
	
	
	$sqlServerSrc = New-Object ("Microsoft.SqlServer.Management.Smo.Server") "secacc01.bussops.ubc.ca"
	$sqlServerDst = New-Object ("Microsoft.SqlServer.Management.Smo.Server") "secacc01-dev.bussops.ubc.ca"

	$smoRestore = new-object("Microsoft.SqlServer.Management.Smo.Restore")

	$smoRestore.Devices.AddDevice("E:\Refresh\AcsData_20111205112647.bak, [Microsoft.SqlServer.Management.Smo.DeviceType]::File)
	$dt = $smoRestore.ReadBackupHeader($sqlServerSrc)

	$dt.Rows[0]["DatabaseName"]
#}

#gci "E:\Refresh" | foreach { a $_.FullName }