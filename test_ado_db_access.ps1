$adOpenStatic = 3
$adLockOptimistic = 3

$dataSource = "A:\Sys.mdb"
$selectQuery = "SELECT * FROM Filter"

$dsn = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=$dataSource;"

$connection = new-object -ComObject ADODB.Connection
$recordSet = new-object -comobject ADODB.Recordset

$connection.Open($dsn)

$recordSet.Open($selectQuery, $connection, $adOpenStatic, $adLockOptimistic)

$recordSet.MoveFirst()

[array]$table = @()

while ($recordSet.EOF -ne $true) {
	[array]$row = @()
	$row += $recordSet.Fields.Item(0).Value
	$row += $recordSet.Fields.Item(1).Value
	$row += $recordSet.Fields.Item(2).Value
	$row += $recordSet.Fields.Item(3).Value
	$row += $recordSet.Fields.Item(4).Value
	$row += $recordSet.Fields.Item(5).Value
	
	$table += $row
	
	$recordSet.MoveNext()
}

$table[0]