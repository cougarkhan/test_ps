workflow wf {
	param ([PSCustomObject[]]$machines)
	
	foreach -parallel ($pc in $machines) {
		if (!(test-connection -ComputerName $pc.NEW_IP -Count 2 -quiet) -eq $true)
		{
			export-csv -InputObject $pc -Path "C:\temp\cct_offline.csv" -Append
		}
	}
}

wf (import-csv "P:\Projects\ION Server Migration\Completed Meters Migrations to BMSUTILNET.csv")