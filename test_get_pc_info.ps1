$supman = import-csv A:\temp\IP\supman.csv | where-object {$_.'Machine Type' -match 'WIndows 2000 Workstation'}
$inventory = new-item "A:\Temp\IP\SUPMAN_INVENTORY.txt" -type file -force

foreach ($pc in $supman) {
    $pc = $pc.DNS.Trim()
    $pcOS = gwmi -class Win32_OperatingSystem -computer $pc
    $pcBIOS = gwmi -class Win32_BIOS -computer $pc
    $pcInstalledApplications = gwmi -class Win32_Product -computer $pc
    $pcScheduledTasks = gwmi -class Win32_ScheduledJob -computer $pc
    
    $pc
    $pcOS
    $pcBIOS
    $pcInstalledApplications
    $pcScheduledTasks
}