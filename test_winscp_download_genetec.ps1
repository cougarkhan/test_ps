[Reflection.Assembly]::LoadFile("$PSScriptRoot\WinSCPnet.dll") | Out-Null

try
{
	$server_address = "ftp.genetec.com"
	$server_port = "21"
	$server_username = "UBC"
	$server_password = "UBC"
	$local_file = "C:\temp\violators_hotlist_upload.txt"
	$remote_file = "violators_hotlist_upload.txt"

	$so = new-object WinSCP.SessionOptions
	$so.Protocol = [WinSCP.Protocol]::Ftp
	$so.HostName = $server_address
	$so.Username = $server_username
	$so.Password = $server_password

	$ss = new-object WinSCP.Session

	try
	{
		$ss.Open($so)
		
		$to = new-object WinSCP.TransferOptions
		$to.PreserveTimestamp = $false
		$to.TransferMode = [WinSCP.TransferMode]::Binary
		
		#$tr = $ss.PutFiles($local_files, $remote_location, $False, $to)
		$tr = $ss.GetFiles($remote_file,$local_file,$false,$to)
		
		$tr.Check()
		
		foreach ($transfer in $tr.Transfers)
		{
			Write-Host ("Download of {0} succeeded" -f $transfer.FileName)
		}
	}
	finally
	{
		$ss.Dispose()
	}

	exit 0
}
catch [Exception]
{
	Write-Host $_.Exception.Message
	exit 1
}