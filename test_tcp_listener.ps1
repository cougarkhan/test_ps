function listen-port ($port) {
	$endpoint = new-object System.Net.IPEndPoint ([ipaddress]::any,$port)
	$listener = new-object System.Net.Sockets.TcpListener $endpoint
	$listener.start()
	$client = $listener.AcceptTcpClient() # will block here until connection
	$stream = $client.GetStream()
	$stream
	#$listener.stop()
}
listen-port 5555

$port=2020
$endpoint = new-object System.Net.IPEndPoint ([IPAddress]::Loopback,$port)
$udpclient=new-Object System.Net.Sockets.UdpClient
$b=[Text.Encoding]::ASCII.GetBytes('Is anyone there?')
$bytesSent=$udpclient.Send($b,$b.length,$endpoint)
$udpclient.Close()

$port=2020
$endpoint = new-object System.Net.IPEndPoint ([IPAddress]::Any,$port)
$udpclient=new-Object System.Net.Sockets.UdpClient $port
$content=$udpclient.Receive([ref]$endpoint)
[Text.Encoding]::ASCII.GetString($content)