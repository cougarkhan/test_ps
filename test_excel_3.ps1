#cp "C:\temp\LAND02_BMSUTILNET_IP_Addresses - Copy.xlsx" "C:\temp\LAND02_BMSUTILNET_IP_Addresses.xlsx" -force
$excel = new-object -com Excel.Application
$excel.visible = $True
$excel.displayalerts=$False
$excel_pid = (Get-Process -Name excel)| Where-Object {$_.MainWindowHandle -eq $excel.HWND}

$wbk = $excel.Workbooks.open("C:\temp\LAND02_BMSUTILNET_IP_Addresses.xlsx")
$wks_main = $wbk.Worksheets.item(1)

$csv = import-csv "C:\Temp\Completed Meters Migrations to BMSUTILNET.csv"

foreach ($item in $csv)
{
	$orig_ip = $item.NEW_IP.Split(".")
	$search_ip = "$($orig_ip[0]).$($orig_ip[1]).$($orig_ip[2]).0"
	$search_range = $wks_main.Range("C1").EntireColumn
	$search_result = $search_range.Find($search_ip)
	
	if ($search_result -ne $null)
	{
		$row = $search_result.Row
		write-host "Found $($search_result.value()) on Row $row in $($wks_main.name)" -foregroundcolor GREEN
		$update_sheet_name = $wks_main.Cells.Item($row,1).Value()
		
		write-host "Opening $update_sheet_name..."
		$wks_update = $wbk.Worksheets.Item($update_sheet_name)
		$search_ip = $item.NEW_IP
		$search_range = $wks_update.Range("A1").EntireColumn
		$search_result = $search_range.Find($search_ip)
		if ($search_result -ne $null)
		{
			$row = $search_result.Row
			write-host "Found $($search_result.value()) on Row $row in $($wks_update.name)" -foregroundcolor GREEN
			if ($wks_update.Cells.Item($row,3).value() -eq $null)
			{
				$wks_update.Cells.item($row,2).value() = "Building Electric Meter"
				$wks_update.Cells.item($row,3).value() = $($item.ION_Building_Name.replace("building.",""))
				$wks_update.Cells.item($row,4).value() = $item.Building_ID
				$wks_update.Cells.item($row,6).value() = $item.Transmogrifier_CCT_Port
				$wks_update.Cells.item($row,7).value() = $item.MAC
				$wks_update.Cells.item($row,8).value() = $item.Switch_Port
				write-host "Updated Row $row in $($wks_update.name)" -foregroundcolor CYAN
			}
			else
			{
				write-host "Record found on Row $row in $($wks_update.name), but not updated.  Cell is not empty" -foregroundcolor RED
			}
		}
		else
		{
			write-host "$search_ip not found in $($wks_update.name)" -foregroundcolor RED
		}
	}
	else
	{
		write-host "$search_ip not found in $($wks_main.name)" -foregroundcolor RED
	}
}
	
$wbk.Save()
$wbk.close()