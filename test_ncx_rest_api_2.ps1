function encode_64 
{ 
	param([string]$encode_me)
	$str = [System.Text.Encoding]::UTF8.GetBytes($encode_me)
	[System.Convert]::ToBase64String($str)
}

$restuser = encode_64 "restapi:S1mpl322!"

$headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
$headers.Add("AnutaAPIVersion","1.0")
$headers.Add("Authorization","Basic $restuser")
$headers.Add("Content-Type","application/json")

$uri = "https://bis-appncx01-dev.bis.it.ubc.ca/rest/activedirectories/action/testconnection.json"

Invoke-RestMethod -Uri $uri -Headers $headers -Method PUT -body @{"url=ldap://ead.ubc.ca","domain=ead.ubc.ca"}