#$machine = read-host "Enter Machine Name:"

param([string]$machine)

if (!$machine) {
    Write-Host "`nMissing value for -machine`n"
    exit
}

$diskArray = get-wmiobject Win32_Logicaldisk -computername $machine

foreach ($disk in $diskArray) {
    if ($disk.DriveType -eq 3 -or $disk.DriveType -eq 4) {
        [int]$size = $disk.Size / (1024 * 1024)
        [int]$free = $disk.FreeSpace / (1024 * 1024)
        
        Write-Host "`nDevice ID: "$disk.DeviceID
        Write-Host "Device Type: "$disk.DriveType
        Write-Host "Description: "$disk.Description
        Write-Host "Disk Space: $free MB / $size MB"
    }
}
Write-Host "`n"