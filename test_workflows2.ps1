param([string[]]$list)

workflow check_java { 
	param([string[]]$computerNames)
	
	foreach -parallel($computer in $ComputerNames) { 
		gcim -query "SELECT * from Win32_Product WHERE name LIKE '%java%'" -pscomputername $computer | select -property * | out-file -filepath "C:\temp\$computer.txt"
	}
}

check_java $list