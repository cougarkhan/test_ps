function Get-InstalledPrograms() {
	param ([string] $computer = '.')
	
	$programs_installed = @{};
	$win32_product = @(get-wmiobject -class 'Win32_Product' -computer $computer);
	foreach ($product in $win32_product) {
		$name = $product.Name;
		$version = $product.Version;
		if ($name -ne $null) {
			$programs_installed.$name = $version;
		}
	}
	return $programs_installed;
}

Get-InstalledPrograms

function Get-InstalledPrograms() {
	param ([string] $computer = '.')
	
	$programs_installed = @{};
	$win32_product = @(get-wmiobject -class 'Win32_Product' -computer $computer -credential (get-credential));
	$f = Get-Date -Format yyyyMMddHHmmss
	$win32_product | select Name, Vendor, PackageName, Version | export-csv "C:\temp\$computer-$f.csv"
}
