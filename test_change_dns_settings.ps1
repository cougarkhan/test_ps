param ( [string[]]$servers )
#$servers = @("ubccard-01","ubccard-03","ubccard-06")
#$servers = read-host "Enter Machine Name"
if ($servers -eq $NULL) { 
	$servers = read-host "Enter Machine Name"
}

foreach($server in $servers) {
	if ((test-connection $server -count 2 -quiet) -eq $true) {
		Write-Host "`nConnecting to $server...`n" -foregroundcolor "GREEN"
		$nics = Get-WmiObject Win32_NetworkAdapterConfiguration -ComputerName $server -ErrorAction Continue | Where{$_.IPEnabled -eq "TRUE"}
		$oldDNS = "137.82.196.72"
		$localDNS = "127.0.0.1"
		$newDNS = "142.103.178.117","142.103.178.118"
		$primWins = ""
		$secWins = ""

		foreach($nic in $nics) {
			if ($nic.DNSServerSearchOrder -ne $NULL ) {
				
				Write-Host "`tExisting IP Address " $nic.IPAddress
				Write-Host "`tExisting DNS Servers " $nic.DNSServerSearchOrder
				Write-Host "`tExisting WINS Servers " $nic.WINSPrimaryServer $nic.WINSSecondaryServer
				Write-Host "`n`tIndex " $nic.Index "`n"
				
				# $dns = $nic.SetDNSServerSearchOrder($newDNS)
				# $wins = $nic.SetWinsServer($primWins,$secWins)

				# if($dns.ReturnValue -eq 0) {
					# Write-Host "`tSuccessfully Changed DNS Servers on " $server
				# }
				# else {
					# Write-Host "`tFailed to Change DNS Servers on " $server
				# }
				
				# if($wins.ReturnValue -eq 0) {
					# Write-Host "`tSuccessfully Changed WINS Servers on " $server
				# }
				# else {
					# Write-Host "`tFailed to Change WINS Servers on " $server
				# }
			} else {
				write-host "No Valid entries found."
			}
		}
	} else {
		write-host "$server is offline" -foregroundcolor RED
	}
}