function global:ADD-LOGFILE {

param(
	[string]$folder = "C:\Temp",
	[string]$preface = "LogFile",
	[string]$extension = ".log"
	)
	
$today = get-date
$date = $today.toshortdatestring().replace("/","")
$time = $today.tostring("HH:mm:ss").replace(":","")

$logfilename = $folder+"\"+$preface+"-"+$date+"-"+$time+$extension

$logfilename
}