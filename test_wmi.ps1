#$coll = Get-Content A:\Temp\PARKING\parking-computers.txt

function fix_wuau {
    param ([string]$strRemoteMachine) 

    (get-wmiobject -computer $strRemoteMachine win32_service -filter "Name='wuauserv'").invokemethod("stopservice",$null)
    Start-Sleep -s 3
    (get-wmiobject -computer $strRemoteMachine win32_service -filter "Name='wuauserv'").State
    reg delete \\$strRemoteMachine\HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\WindowsUpdate /v AccountDomainSid /f
    reg delete \\$strRemoteMachine\HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\WindowsUpdate /v PingID /f
    reg delete \\$strRemoteMachine\HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\WindowsUpdate /v SusClientId /f
    (get-wmiobject -computer $strRemoteMachine win32_service -filter "Name='wuauserv'").invokemethod("startservice",$null)
    Start-Sleep -s 3
    (get-wmiobject -computer $strRemoteMachine win32_service -filter "Name='wuauserv'").State
    
    $strCommand = "wuauclt /resetauthorization /detectnow"
    $strUser = "bussops\bopsis"
    $strPassword = "Fl3x!bl3"
    $objProcess = [WmiClass]"\\$strRemoteMachine\ROOT\CIMV2:Win32_Process"
    
    $objProcess.psbase.Scope.Options.userName=$strUser
    $objProcess.psbase.Scope.Options.Password=$strPassword
    $objProcess.psbase.Scope.Options.Impersonation = [System.Management.ImpersonationLevel]::Impersonate
    $objProcess.psbase.Scope.Options.Authentication = [System.Management.AuthenticationLevel]::PacketPrivacy
    
    $objProcess.Create($strCommand)
    $objProcess.ProcessId
    $objProcess.ReturnValue
    
    #$strResult = ([WmiClass]"\\$strRemoteMachine\ROOT\CIMV2:Win32_Process").create("cmd /c wuauclt /resetauthorization /detectnow")
    #$strResult
}

$colComputers = @("csec-141")

foreach ($objComputer in $colComputers) {
    $objPing = new-object System.Net.NetworkInformation.Ping
    $strReply = $objPing.send($objComputer)
    if ($strReply.status –eq “Success”) {
        fix_wuau $objComputer    
    }    
}

