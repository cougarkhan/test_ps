param ([string] $b, [int] $m=10, [int]$d=3)

#############################################################################
#
# Check Parameters
#
#############################################################################
function func_Help() {
    Write-Host "HELP!"
    exit
}


if(!$b) {
    Write-Host "Backup Location not set!" foregroundcolor "RED"
    func_Help()
} else if (!(test-path $b)) {
    Write-host "Backup Location not valid!" foregroundcolor "RED"
    func_Help()
}

function return_Date {
    # Format Date Strings
    $seconds = (get-date).Second
    if ($seconds.length -eq 1) { $seconds = "0$seconds" }

    $minute = (get-date).Minute
    if ($minute.length -eq 1) { $minute = "0$minute" }

    $hour = (get-date).Hour
    if ($hour.length -eq 1) { $hour = "0$hour" }

    $day = (get-date).day.tostring()
    if ($day.length -eq 1) { $day = "0$day" }

    $month = (get-date).month.tostring()
    if ($month.length -eq 1) { $month = "0$month" }

    $year = (get-date).year.tostring()

    $new_Date = "$year$month$day"
    
    return $new_Date
}

$log_date = return_Date()
$file_Age_Limit = (get-date).AddDays(-$d)

$child_Folders = gci $parent_Folder | where {$_.PSIsContainer -eq $true}

foreach ($folder in $child_Folders {

    if ($folder) {
        $files = gci $folder | where {$_.PSIsContainer -ne $true }
    }
    
    $files
}