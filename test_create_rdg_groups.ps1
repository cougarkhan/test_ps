$bsp = "https://bis.share.ubc.ca/AdminOps/_api/web/lists/"
$bsplimit = "?`$top=1000"
$bspas = "$($bsp)GetByTitle('AllServers')/items$bsplimit"
$bspdata = @("ID","Title","OS","IP_ADDRESS","DNS_NAME","Interface","ENVIRONMENT","SPEEDCHART","INFRASTRUCTURE","LOCATION","Dept","Remarks","Pro cessor_x0020_Type","Modified","Created")
$win = Get-SharePointObject -Uri $bspas -Columns $bspdata | where { $_.OS -like "*window*" }


#$groups =  $win | select -exp group | sort -unique
#$environments = $win | select -exp environment | sort -unique

$file_xml_template_open = "<?xml version=`"1.0`" encoding=`"utf-8`"?>
<RDCMan schemaVersion=`"1`">
	<version>2.2</version>
	<file>
		<properties>
			<name>Windows Update</name>
			<expanded>True</expanded>
			<comment />
			<logonCredentials inherit=`"FromParent`" />
			<connectionSettings inherit=`"FromParent`" />
			<gatewaySettings inherit=`"FromParent`" />
			<remoteDesktop inherit=`"FromParent`" />
			<localResources inherit=`"FromParent`" />
			<securitySettings inherit=`"FromParent`" />
			<displaySettings inherit=`"FromParent`" />
		</properties>"
$file_xml_template_close = "	</file>
</RDCMan>"

function group_xml_template_open {

	param ([string]$group_name)

	$group_xml_template_open = "		<group>
				<properties>
					<name>$group_name</name>
					<expanded>False</expanded>
					<comment />
					<logonCredentials inherit=`"FromParent`" />
					<connectionSettings inherit=`"FromParent`" />
					<gatewaySettings inherit=`"FromParent`" />
					<remoteDesktop inherit=`"FromParent`" />
					<localResources inherit=`"FromParent`" />
					<securitySettings inherit=`"FromParent`" />
					<displaySettings inherit=`"FromParent`" />
				</properties>"
	$group_xml_template_open | add-content "C:\temp\servers\rdg\Windows_Servers.rdg"
}			

function group_xml_template_close {
	$group_xml_template_close = "		</group>"
	$group_xml_template_close | add-content "C:\temp\servers\rdg\Windows_Servers.rdg"
}

function sub_group_xml_template_open {

	param ([string]$sub_group_name)

	$sub_group_xml_template_open = "			<group>
					<properties>
						<name>$sub_group_name</name>
						<expanded>False</expanded>
						<comment />
						<logonCredentials inherit=`"FromParent`" />
						<connectionSettings inherit=`"FromParent`" />
						<gatewaySettings inherit=`"FromParent`" />
						<remoteDesktop inherit=`"FromParent`" />
						<localResources inherit=`"FromParent`" />
						<securitySettings inherit=`"FromParent`" />
						<displaySettings inherit=`"FromParent`" />
					</properties>"
	
	$sub_group_xml_template_open | add-content "C:\temp\servers\rdg\Windows_Servers.rdg"
}

function sub_group_xml_template_close {
	$sub_group_xml_template_close = "		</group>"
	$sub_group_xml_template_close | add-content "C:\temp\servers\rdg\Windows_Servers.rdg"
}

function server_xml_template {

	param([string]$server_name)

	$server_xml_template = "				<server>
							<name>$server_name</name>
							<displayName>$server_name</displayName>
							<comment />
							<logonCredentials inherit=`"FromParent`" />
							<connectionSettings inherit=`"FromParent`" />
							<gatewaySettings inherit=`"FromParent`" />
							<remoteDesktop inherit=`"FromParent`" />
							<localResources inherit=`"FromParent`" />
							<securitySettings inherit=`"FromParent`" />
							<displaySettings inherit=`"FromParent`" />
						</server>"
	$server_xml_template | add-content "C:\temp\servers\rdg\Windows_Servers.rdg"
}

$file_xml_template_open | set-content "C:\temp\servers\rdg\Windows_Servers.rdg"
									
foreach ($group in $groups) {
	$servers = $win | where { $_.group -eq $group }
	$group_name = $group
	group_xml_template_open $group_name
	
	foreach ($environment in $environments) {
		if ($sub_servers = [bool]$($servers | where {$_.environment -eq $environment}) -eq $true) {
			$sub_group_name = $environment
			$sub_servers = ($servers | where {$_.environment -eq $environment})
			sub_group_xml_template_open $sub_group_name
			
			foreach ($server in $sub_servers) {
				$server_name = $server.dnshostname
				server_xml_template $server_name
			}
			
			sub_group_xml_template_close
		}
	}
	
	group_xml_template_close
}

$file_xml_template_close | add-content "C:\temp\servers\rdg\Windows_Servers.rdg"