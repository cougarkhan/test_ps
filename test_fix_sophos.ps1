function log_time {
	return Get-Date -Format HH:mm:ss:ffff
}

function stopAndWaitForService {
	param ([System.ServiceProcess.ServiceController]$service)
	
	$serviceName = $service.Name
	$serviceMachine = $service.MachineName
	$timeout = new-timespan -seconds 60
	
	# Stop service on destination machine
	$timeResult = measure-command {
		write-host (log_time)"`tStopping $serviceName on $serviceMachine..."
		write-host (log_time)"`t$serviceName status is: " -nonewline
		$service.stop()
		try {
			$service.waitforstatus([system.ServiceProcess.ServiceControllerStatus]::Stopped, $timeout)
		} catch [System.ServiceProcess.TimeoutException] {
			write-host "`n"
			write-host (log_time)"`tTimeout Exceeded before Service Stopped.  The service may still have stopped."
		} finally {
			$service.refresh()
			$status = $service.status
			write-host "$status"
		}			
	}
	
	write-host (log_time)"`tCompleted Stopping Service. $timeResult"
	
	if ($service.status -eq [system.ServiceProcess.ServiceControllerStatus]::Stopped) {
		return $true
	} else {
		return $false
	}
}

function startAndWaitForService {
	param ([System.ServiceProcess.ServiceController]$service)
	
	$serviceName = $service.Name
	$serviceMachine = $service.MachineName
	$timeout = new-timespan -seconds 60
	
	# Stop service on destination machine
	$timeResult = measure-command {
		write-host (log_time)"`tStarting $serviceName on $serviceMachine..."
		$service.start()
		write-host (log_time)"`t$serviceName status is: " -nonewline
		try {
			$service.waitforstatus([system.ServiceProcess.ServiceControllerStatus]::Running, $timeout)
		} catch [System.ServiceProcess.TimeoutException] {
			write-host "`n"
			write-host (log_time)"`tTimeout Exceeded before Service Started.  The service may still have started."
		} finally {
			$service.refresh()
			$status = $service.status
			write-host "$status"
		}
	}
	
	write-host (log_time)"`tCompleted Starting Service. $timeResult"
	
	if ($service.status -eq [system.ServiceProcess.ServiceControllerStatus]::Running) {
		return $true
	} else {
		return $false
	}
}

$servers = gc "C:\temp\sophos.txt"
$savServices = gc "C:\temp\savservice.txt"
$sophosBinaries = "C:\temp\sophos\Sophos\*"

foreach ($server in $servers) {
	$ss = get-service -name "SAVAdminService" -computername $server
	stopAndWaitForService $ss
	cp -recurse $sophosBinaries "\\$server\c$\Program Files (x86)\Sophos\" -verbose
	startAndWaitForService $ss
	$ss = get-service -name "SAVService" -computername $server
	stopAndWaitForService $ss
	cp -recurse $sophosBinaries "\\$server\c$\Program Files (x86)\Sophos\" -verbose
	startAndWaitForService $ss
	$ss = get-service -name "Sophos Agent" -computername $server
	stopAndWaitForService $ss
	cp -recurse $sophosBinaries "\\$server\c$\Program Files (x86)\Sophos\" -verbose
	startAndWaitForService $ss
	$ss = get-service -name "Sophos AutoUpdate Service" -computername $server
	stopAndWaitForService $ss
	cp -recurse $sophosBinaries "\\$server\c$\Program Files (x86)\Sophos\" -verbose
	startAndWaitForService $ss
	$ss = get-service -name "Sophos Message Router" -computername $server
	stopAndWaitForService $ss
	cp -recurse $sophosBinaries "\\$server\c$\Program Files (x86)\Sophos\" -verbose
	startAndWaitForService $ss
	$ss = get-service -name "Sophos Web Control Service" -computername $server
	stopAndWaitForService $ss
	cp -recurse $sophosBinaries "\\$server\c$\Program Files (x86)\Sophos\" -verbose
	startAndWaitForService $ss
	$ss = get-service -name "swi_service" -computername $server
	stopAndWaitForService $ss
	cp -recurse $sophosBinaries "\\$server\c$\Program Files (x86)\Sophos\" -verbose
	startAndWaitForService $ss
}