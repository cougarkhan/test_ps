Import-Module SharePointObjects
$bsp = "https://bis.share.ubc.ca/AdminOps/_api/web/lists/"
$bsplimit = "?`$top=1000"
$bspas = "$($bsp)GetByTitle('AllServers')/items$bsplimit"
$bspdata = @("ID","Title","OS","IP_ADDRESS","DNS_NAME","Interface","ENVIRONMENT","SPEEDCHART","INFRASTRUCTURE","LOCATION","Dept","Remarks","Processor_x0020_Type","Modified","Created")
$servers = Get-SharePointObject -Uri $bspas -Columns $bspdata | where { $_.OS -like "*window*" }
$file_name = "C:\temp\servers\rdg\Windows_Servers.rdg"
$file_xml_template_open = "<?xml version=`"1.0`" encoding=`"utf-8`"?>
<RDCMan schemaVersion=`"1`">
	<version>2.2</version>
	<file>
		<properties>
			<name>Windows Update</name>
			<expanded>True</expanded>
			<comment />
			<logonCredentials inherit=`"FromParent`" />
			<connectionSettings inherit=`"FromParent`" />
			<gatewaySettings inherit=`"FromParent`" />
			<remoteDesktop inherit=`"FromParent`" />
			<localResources inherit=`"FromParent`" />
			<securitySettings inherit=`"FromParent`" />
			<displaySettings inherit=`"FromParent`" />
		</properties>"
$file_xml_template_close = "	</file>
</RDCMan>"
function server_xml_template {

	param([string]$server_name)

	$server_xml_template = "			<server>
				<name>$server_name</name>
				<displayName>$server_name</displayName>
				<comment />
				<logonCredentials inherit=`"FromParent`" />
				<connectionSettings inherit=`"FromParent`" />
				<gatewaySettings inherit=`"FromParent`" />
				<remoteDesktop inherit=`"FromParent`" />
				<localResources inherit=`"FromParent`" />
				<securitySettings inherit=`"FromParent`" />
				<displaySettings inherit=`"FromParent`" />
			</server>"
	$server_xml_template | add-content $file_name
}

$file_xml_template_open | set-content $file_name

foreach ($server in $servers) {
	server_xml_template $($server.DNS_NAME)
}

$file_xml_template_close | add-content $file_Name