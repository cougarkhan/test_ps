param ([string]$name, [string]$filePath, [switch]$recurse, [switch]$check)

start-transcript C:\temp\SUPMAN_ACL_SM-ADMIN_ADD.log

if ($name -eq "") { 
	write-host "Username missing."
	exit
} 
if ($filePath -eq "") {
	write-host "File Path missing."
	exit
}

Import-Module ActiveDirectory

$userName = get-adobject -filter "sAMAccountName -eq '$name'" -Properties objectSid
$permission = "BUSSOPS\SM-Admin","FullControl","ContainerInherit,ObjectInherit","None","Allow"
$accessRule = new-object System.Security.AccessControl.FileSystemAccessRule $permission

[array]$fileList = @()

write-host "Building File List..." -nonewline
if ($recurse -eq $true) {
	$fileList = ls $filePath -directory -recurse
} else {
	$fileList = ls $filePath -directory
}
write-host "Done" -nonewline
$userSID = $userName.objectSid.Value

foreach ($item in $fileList) {
	$aclFound = 0
	$itemACL = get-acl $item.FullName
	foreach($access in $itemACL.Access) {
		$SID = $access.IdentityReference.Translate([System.Security.Principal.SecurityIdentifier])

		if ($SID.BinaryLength -ge 28)
		{
			$itemSID = $SID.Value
			
			if ($itemSID -eq $userSID) {
				$aclFound = 1
			}
		}
	}
	
	if ($aclFound -eq 0) {
		if ($check -eq 0) {
			$itemACL.AddAccessRule($accessRule)
			write-host "Adding $permission TO $($item.FullName)"
			try {
				$itemACL | set-acl $item.FullName
			} catch {
				write-host "ERROR: Apply ACL Failed on $($item.FullName)"
			}
		} else {
			$item.FullName
		}
	}
}

stop-transcript