$networkAdapters = gwmi Win32_NetworkAdapterConfiguration -filter "IPEnabled=TRUE"
$nacBind = [wmiclass]"Win32_NetworkAdapterConfiguration"
write-host "Updating DNS Server Entries"
$dssoResult = $networkAdapters.SetDNSServerSearchOrder($network.dns.server)
$dssoResult.ReturnValue
write-host "Updating DNS Suffix Search Order"
[array]$suffixes = $network.dns.suffix
$dssoResult = $nacBind.SetDNSSuffixSearchOrder($suffixes)
$dssoResult.ReturnValue