# Load WinSCP .NET assembly
# Use "winscp.dll" for the releases before the latest beta version.
#[Reflection.Assembly]::LoadFrom("WinSCPnet.dll") | Out-Null
[Reflection.Assembly]::LoadFile("E:\powershell\Branch01\Test\WinSCPnet.dll") | Out-Null
# Session.FileTransferProgress event handler
 
function FileTransferProgress
{
    Param($e)
 
    # New line for every new file
    if (($script:lastFileName -ne $Null) -and
        ($script:lastFileName -ne $e.FileName))
    {
        Write-Host
    }
 
    # Print transfer progress
    Write-Host -NoNewline ("`r{0} ({1:P0})" -f $e.FileName, $e.FileProgress)
 
    # Remember a name of the last file reported
    $script:lastFileName = $e.FileName
}
 
# Main script
 
$script:lastFileName = $Null
 
try
{
    $sessionOptions = New-Object WinSCP.SessionOptions
    $sessionOptions.Protocol = [WinSCP.Protocol]::Ftp
    $sessionOptions.HostName = "obsidian-ami.com"
    $sessionOptions.UserName = "qmcobvius_ponderosawest"
    $sessionOptions.Password = "nu2uri65"
 
    $session = New-Object WinSCP.Session
    try
    {
        # Will continuously report progress of transfer
        $session.add_FileTransferProgress( { FileTransferProgress($_) } )
 
        # Connect
        $session.Open($sessionOptions)
		
		$to = new-object WinSCP.TransferOptions
		$to.TransferMode = [WinSCP.TransferMode]::Binary
		
		$transfer = $ss.PutFiles("C:\temp\west", "/", $False, $to)
		
		$transfer.Check()
		
		foreach ($transfer in $tr.Transfers)
		{
			Write-Host ("Upload of {0} succeeded" -f $transfer.FileName)
		}
    }
    finally
    {
        # Terminate line after the last file (if any)
        if ($script:lastFileName -ne $Null)
        {
            Write-Host
        }
 
        # Disconnect, clean up
        $session.Dispose()
    }
 
    exit 0
}
catch [Exception]
{
    Write-Host $_.Exception
    exit 1
}