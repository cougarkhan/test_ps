$updateSession = new-object -com Microsoft.update.Session
$updateSearcher = $updateSession.CreateUpdateSearcher()
$searchParameters = "IsInstalled=0 and ISHidden=0"
$searchResult = $updateSearcher.search($searchParameters)

if ($searchResult.Updates.Count -le 0) {
	Write-Host "System is Up to Date."
	exit
}

$tableFormat1 = @{Expression={"{0:N2}" -f ($_.MaxDownloadSize / (1024*1024)) + " MB"};Label="Download Size"}, @{Expression={"KB" + $_.KBArticleIDs};Label="KB Article"}, @{Expression={$_.Title};Label="Title";width=25}
				
Write-Host "`n`nThe following updates are available for download...`n"
$searchResult.Updates | FT $tableFormat1 -autosize

[double]$downloadSize = 0
$rebootNeeded = @()

foreach ($item in $searchResult.Updates) {
	$downloadSize += [double]$item.MaxDownloadSize
	if ($item.RebootRequired -eq $true) {
		$rebootNeeded += $item.KBArticleIDs
	}
}

$downloadSize = $downloadSize / (1024*1024)

Write-Host "`n`nTotal Download Size is $downloadSize MB`n"
Write-Host $rebootNeeded.Count "of" $searchResult.Updates.Count "updates will require a system restart.`n"
if ($rebootNeeded.Count -gt 0) {
	Write-Host "The following updates will require a system restart..."
}
Write-Host $rebootNeeded -foregroundcolor YELLOW

$answer = ""

do {
	$answer = Read-Host "`nInstall Updates? (y/n/c)"
} until ($answer -imatch "y" -OR $answer -imatch "n" -OR $answer -imatch "c")


switch ($answer) {
	"y" {
			Write-Host "`nInstalling Updates...`n`n"
	
			$updatesToInstall = new-object -com Microsoft.update.UpdateColl
			foreach ($update in $searchResult.Updates) {
				Write-Host $updatesToInstall.Add($update) "KB"$update.KBArticleIDs "Added to Installation Collection."
			}		
			
	
			if ($updateInstaller = $updateSession.CreateUpdateInstaller()) {
				Write-Host "Update Installer object created...`n"
			}
			
			$updateInstaller.Updates = $updatesToInstall
			$installationResult = $updateInstaller.Install()
			#$updateInstaller.Install()
			
			Write-Host "`nInstallation Result" $installationResult.ResultCode
			Write-Host "Reboot Required:" $installationResult.RebootRequired
			Write-Host "`nList of installed updates and results:"
			
			for ($i=0; $i -lt $updatesToInstall.Count; $i++) {
				Write-Host $updatesToInstall.Item($i).Title":" $installationResult.GetUpdateResult($i).ResultCode
			}
			
		}
	"n" { exit }
	"C" { 
			$updatesToInstall = new-object -com Microsoft.update.UpdateColl
			foreach ($update in $searchResult.Updates) {
				$kbArticle = $update.KBArticleIDs
				$kbTitle = $update.Title
				
				do {
					$answer2 = Read-Host "Add (KB$kbArticle) $kbTitle to Installation Collection? (y/n)"
				} until ($answer2 -imatch "y" -OR $answer2 -imatch "n")
				
				if ($answer2 -imatch "y") {
					Write-Host $updatesToInstall.Add($update) "KB"$update.KBArticleIDs "Added to Installation Collection."
				}
			}
			
			Write-Host "`nUpdates to be installed:`n"
			$updatesToInstall | FT $tableFormat1 -autosize
			
		}
	default {
				Write-Host "Does Not Compute...DANGER! DANGER! DANGER!!" 
				exit
			}
}
