add-type @"
using System.Net;
using System.Security.Cryptography.X509Certificates;

	public class IDontCarePolicy : ICertificatePolicy {
	public IDontCarePolicy() {}
	public bool CheckValidationResult(
		ServicePoint sPoint, X509Certificate cert,
		WebRequest wRequest, int certProb) {
		return true;
	}
}
"@
[System.Net.ServicePointManager]::CertificatePolicy = new-object IDontCarePolicy 

function encode64
{
	param ([string] $stringToEncode)

	$asBytesArray = [System.Text.Encoding]::UTF8.GetBytes($stringToEncode)
	$convertedString = [System.Convert]::ToBase64String($asBytesArray)

	return $convertedString
}

function decode64
{
	param ([string] $stringToDecode)

	$asBytesArray = [System.Convert]::FromBase64String($stringToDecode)
	$convertedString = [System.Text.Encoding]::UTF8.GetString($asBytesArray)

	return $convertedString
}

$headers = @{}
$headers.Add("AnutaAPIVersion","1.0")
$headers.Add("Authorization","Basic bXBhZG1pbjpTMW1wbDMyMiFLcjhrM245MTE=")
Invoke-webrequest -Uri "https://bis-appncx01-dev.bis.it.ubc.ca/rest/devices.json" -Headers $headers -Method Get
Invoke-restmethod -Uri "https://bis-appncx01-dev.bis.it.ubc.ca/rest/devices.json" -Headers $headers -Method Get

