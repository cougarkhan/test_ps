##############################################################################################
#
# ORACLE BACKUP VERIFICATION
#
##############################################################################################
#
# This script will check 3 conditions to see if the oracle backup occurred.
# 
# 1. It will check whether the scheduled task for the Oracle Daily Backup ran successfully.
# 2. It will check to see if there are files from the current script run day in the 
#    specified backup location.
# 3. It will compare the current and last backup file sizes and send an alert if the margin is
#    off by 15% (default) or a margin specified by the -m command
#
# -b    Specifies the backup location to search in for backups.
# -t    The name of the Scheduled Daily Task
# -m    The margin by which the file size can differ in a comparison
#
##############################################################################################
#
# Script written by Michael Pal on Sept 28th 2011
#
##############################################################################################

param ( [string]$b,
        [string]$t="Oracle Daily Backup",
        [validaterange(0.01,0.99)][float]$m=0.15)
        
function send_email([string]$err_msg="There was an error.") {
    # SMTP Server
    $smtpServer = "mail-relay.ubc.ca"

    # Create new SMTP object to send email
    $smtp = new-object Net.Mail.SmtpClient($smtpServer)

    # Send the Email
    $from = $Env:COMPUTERNAME + "@bussops.ubc.ca"
    $to = "michael.pal@ubc.ca"
    $subject = "Backup Errors on " + $Env:COMPUTERNAME 
    $body = "It's down SON!`n`n"

    $smtp.Send($from, $to, $subject, $body)
}

# Format Date Strings
$seconds = (get-date).Second
if ($seconds.length -eq 1) { $seconds = "0$seconds" }

$minute = (get-date).Minute
if ($minute.length -eq 1) { $minute = "0$minute" }

$hour = (get-date).Hour
if ($hour.length -eq 1) { $hour = "0$hour" }

$day = (get-date).day.tostring()
if ($day.length -eq 1) { $day = "0$day" }

$month = (get-date).month.tostring()
if ($month.length -eq 1) { $month = "0$month" }

$year = (get-date).year.tostring()


$backupDate = "$year$month$day"

        
$error_log = @()
$backup_log = @()
$backup_log_loc = "\\bops-file1\JobLogs\$backupDate-$Env:COMPUTERNAME-Oracle_Backup.txt"

#####################################################################################
# Did the Scheduled task run successfully?  If not send an error email.
#####################################################################################
$sched_task_srv = new-object -com ("Schedule.Service")
$sched_task_srv.connect()
$sched_root_folder = $sched_task_srv.GetFolder("\")
$sched_tasks = $sched_root_folder.GetTasks(0)
$oracle_task = $sched_tasks | where {$_.Name -like $t}

if ($oracle_task.LastTaskResult -ne 0) {
    $error_log =+ "The Oracle Daily Backup scheduled task had errors and did not complete.`n`nThe Last Task Result was " + $oracle_task.LastTaskResult
    send_email($error_log)
    exit
} else {
    $backup_log =+ "The Scheduled Tasks $oracle_task.Name compelted at $oracle_task.LastRunTime with the Result $oracle_task.LastTaskResult.`n"
    $backup_log =+ "Name: `t $oracle_task.Name"
    $backup_log =+ "Enabled: `t $oracle_task.Enabled"
    $backup_log =+ "LastRunTime: `t $oracle_task.LastRunTime"
    $backup_log =+ "LastTaskResult: `t $oracle_task.LastTaskResult"
    $backup_log =+ "NumberOfMissedRuns: `t $oracle_task.NumberOfMissedRuns"
    $backup_log =+ "NextRunTime: `t $oracle_task.NextRunTime`n`n`n"
}

#####################################################################################
# Check for the existence of the latest backup.  If not send an error email.
#####################################################################################
if (!($current_backups = gci $b | where {$_.creationtime.days -eq (get-date).days})) {
    $error_log =+ "Backup file for " + (get-date) + " is missing!  Backup job may not have ran."
    send_email($error_log)
    exit
} else {
    $backup_log =+ "Found the following new backup files:`n"
    $backup_log =+ $current_backups
}


#####################################################################################
# If latest backup exists then check it's size against the last backup.
# If the size is off by 15% or the percentage specified by the -m flag then
# send an error email.
# 
# Currently not possible since Oracle backups have a randomly generated name 
# for each file.
#####################################################################################

#$current_backups = gci $b | where {$_.creationtime.days -eq (get-date).days}
#$last_backups = gci $b | where {$_.creationtime.days -eq (get-date).AddDays(-1)}