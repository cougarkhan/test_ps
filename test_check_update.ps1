#$machines = Get-ADComputer -filter "OperatingSystem -like 'Windows*'" -properties Name | select Name
#remove-item c:\temp\machines.txt
#$machines | %{ $_.name | out-file C:\temp\machines.txt -append }
$32bit = gc "C:\temp\machines.txt" | gcim -query "select * from Win32_Processor where DeviceID='CPU0' and AddressWidth='32'" -operationtimeoutsec 10 -erroraction SilentlyContinue
$32bit | %{
	if (test-connection $_.PSComputerName -count 1 -quiet) {
		$name = $_.PSComputerName
		invoke-command -computername $name -scriptblock {
			Function get-Unixdate ($UnixDate) {
				[timezone]::CurrentTimeZone.ToLocalTime(([datetime]'1/1/1970').AddSeconds($UnixDate))
			}
			$lut = (get-itemproperty HKLM:\SOFTWARE\Sophos\AutoUpdate\UpdateStatus -name lastupdatetime).lastupdatetime 
			
			$lut2 = get-Unixdate $lut
			$name = (gci env:computername).value
			write-host "$name : $lut2"
		}
	}
}

$64bit = gc "C:\temp\machines.txt" | gcim -query "select * from Win32_Processor where DeviceID='CPU0' and AddressWidth='64'" -operationtimeoutsec 10 -erroraction SilentlyContinue
$64bit | %{
	if (test-connection $_.PSComputerName -count 1 -quiet) {
		$name = $_.PSComputerName
		invoke-command -computername $name -scriptblock {
			Function get-Unixdate ($UnixDate) {
				[timezone]::CurrentTimeZone.ToLocalTime(([datetime]'1/1/1970').AddSeconds($UnixDate))
			}
			$lut = (get-itemproperty HKLM:\SOFTWARE\Wow6432Node\Sophos\AutoUpdate\UpdateStatus -name lastupdatetime).lastupdatetime 
			
			$lut2 = get-Unixdate $lut
			$name = (gci env:computername).value
			write-host "$name : $lut2"
		}
	}
}