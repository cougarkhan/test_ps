# Load Assemblies
[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
[System.Reflection.Assembly]::LoadWithPartialName("System.Drawing")

# Init Form
$form = new-object System.Windows.Forms.Form
$form.width = 400
$form.height = 600
$form.text = "Select Something"

$Form.Add_Shown({$Form.Activate()})
$Form.ShowDialog()