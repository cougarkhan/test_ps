param ([string[]]$servers=@((gci env:COMPUTERNAME).value),[switch]$ad)

import-module ActiveDirectory

if ($ad) {
	$servers = Get-ADComputer -filter {Operatingsystem -like "Windows Server *"} | select -exp DNSHostName
}
$log_table = @()
$i = 1

foreach ($server in $servers) {
	$count = $servers.count
	write-progress -activity "Gathering Event Logs..." -status "Processing $server.   $i/$count" -percentcomplete ($i / $servers.count * 100)
	
	$time_result_1 = measure-command { $application_log = get-eventlog -ComputerName $server -logname Application -newest 1000 }
	$time_result_2 = measure-command { $system_log = get-eventlog -ComputerName $server -logname System -newest 1000 }
	
	$log_table += @($server,$time_result_1,"Application",$application_log,$time_result_2,"System",$system_log)
	
	$i++

}

$day = ((date).AddDays(-2)).day
$month = ((date).AddDays(-2)).month
$format = "{0,-20} {1,-40} {2,-15} {3,-12} {4,-12}"

$format -f "Time","Server","Log Name","Warnings","Errors"

for ($x=0;$x -lt $log_table.count;$x = $x+8) {
$format -f $server,"Application",$application_log_warnings.count,$application_log_errors.count	
$format -f $log_table[],$log_table[],$log_table[],$log_table[],$log_table[],
}