param ($sessions)

$pac = gc C:\temp\pac.txt

#$sessions = new-pssession $pac

foreach ($session in $sessions) {
	write-host "Checking $($session.ComputerName) ..."
	invoke-command -session $session -scriptblock {
		$ErrorActionPreference = "SilentlyContinue"
		$null = New-PSDrive -Name HKU -PSProvider Registry -Root Registry::HKEY_USERS
		$hklmODBCsa_scan = "HKLM:\Software\ODBC\ODBC.INI\sa_scan"
		$hklmODBCbak_scan = "HKLM:\Software\ODBC\ODBC.INI\bak_scan"
		(get-itemproperty $hklmODBCsa_scan).PSChildName
		(get-itemproperty $hklmODBCbak_scan).PSChildName
	}
}
