#$network = gc "A:\temp\pac_machines2.txt"
$network = "PAC-199"
$nic = @()

foreach ($machine in $network) {
    $machine = $machine.Trim()
    $ping = new-object System.Net.Networkinformation.Ping
    $pingResult = $ping.send("$machine")
    
    if ($pingResult.status -match "Success") {
       $nic += (gwmi -class Win32_PerfFormattedData_Tcpip_NetworkInterface -computername $machine)
       
    }
}

$nic | where {$_.Name -notlike "*MS TCP Loopback interface*"} | `
Select __SERVER, Name, CurrentBandWidth | Sort-Object CurrentBandWidth | `
Format-Table -AutoSize @{Expression={$_.__SERVER}; Label="Machine" }, `
@{Expression={$_.Name}; Label="NIC" }, `
@{Expression={$_.CurrentBandWidth/ 1000000 }; Label="MBit/s"} 
# | tee -file "A:\Temp\PAC_LINK_SPEED_REPORT_4.txt"

#$result = gwmi -class Win32_PerfFormattedData_Tcpip_NetworkInterface | Select __SERVER, Name, CurrentBandWidth | Format-Table -AutoSize @{Expression={$_.__SERVER}; Label="Machine" },@{Expression={$_.Name}; Label="NIC" },@{Expression={$_.CurrentBandWidth/ 1000000 }; Label="MBit/s"}