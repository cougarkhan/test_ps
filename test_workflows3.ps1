$machines = gc C:\drs\scripts\lbs-sophos.txt

workflow lbs_sophos 
{

param ([string[]]$list)

$obj_arr = @()

foreach -parallel($pc in $list)
{
	$obj = new-object -TypeName system.object
	
	Add-Member -InputObject $obj -type NoteProperty -Name hostname -value $pc
	Add-Member -InputObject $obj -type NoteProperty -Name session -value $(
		try
		{
			new-pssession -computername $pc -erroraction stop
		}
		catch
		{
			"error"
		}
	)
	
	$WORKFLOW:obj_arr += $obj
	
	$obj = $null
}

$obj_arr

}

lbs_sophos $machines
