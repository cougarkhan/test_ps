param([string]$server,[string]$

$c = Get-Credential

$username = $c.username 
$password = $c.GetNetworkCredential().password

$connectionString = [string]::Format( "server={0};database={1};uid={2};pwd={3};", "bis-sqlict-tst.bussops.ubc.ca", "ProtegeGX",$username,$password) 
#open database connection to SQL Server

Write-Host  $connectionString
$conn = New-Object system.Data.SqlClient.SqlConnection
$conn.connectionstring = $connectionString
try {
	$conn.open()
} catch {
	Write-Host "A Connection Error has occurred"
}

switch ($conn.State)
{
	"Open" { Write-Host "Do some work"; }
	Default { Write-Host "The connection is $($conn.State).  There has been an error connecting to the database."; }
}