param ([string[]]$servers=@((gci env:COMPUTERNAME).value),[switch]$ad)

import-module ActiveDirectory

if ($ad) {
	$servers = Get-ADComputer -filter {Operatingsystem -like "Windows Server *"} | select -exp DNSHostName
}

workflow get_events {

	param ([string[]]$servers)

	$day = ((date).AddDays(-2)).day
	$month = ((date).AddDays(-2)).month
	$format = "{0,-40} {1,-15} {2,-12} {3,-12}"

	$format -f "Server","Log Name","Warnings","Errors" | set-content -path C:\temp\test.txt

	foreach -parallel($server in $servers) {
		$event_array = @()
		$log_array = @()
		$application_log_errors = @()
		$application_log_warnings = @()
		$system_log_errors = @()
		$system_log_warnings = @()


		$application_log = get-eventlog -PSComputerName $server -logname Application -newest 5000
		$system_log = get-eventlog -PSComputerName $server -logname System -newest 5000
		
		foreach ($entry in $application_log) {
			if ($entry.timegenerated.day -gt $day -AND $entry.timegenerated.month -eq $month){
				if ($entry.entrytype -match "Error") {
					$application_log_errors += $entry
				}
				if ($_.entrytype -match "Warning") {
					$application_log_warnings += $entry
				}
			} 
		}
		$log_array += $application_log_errors
		$log_array += $application_log_warnings

		$format -f $server,"Application",$application_log_warnings.count,$application_log_errors.count | add-content -path C:\temp\test.txt
		
		foreach ($entry in $system_log) { 
			if ($entry.timegenerated.day -gt $day -AND $entry.timegenerated.month -eq $month){
				if ($entry.entrytype -match "Error") {
					$system_log_errors += $entry
				}
				if ($entry.entrytype -match "Warning") {
					$system_log_warnings += $entry
				}
			} 
		}
		$log_array += $system_log_errors
		$log_array += $system_log_warnings
		
		$format -f $server,"System",$system_log_warnings.count,$system_log_errors.count | add-content -path C:\temp\test.txt

	}
}
get_events $servers