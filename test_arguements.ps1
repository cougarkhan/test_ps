#####################################################################################################################
#
# Script to Alert of HDD Failure
# Log files will be written to \\bops-file1\Server Failures\
#
# Create on 2011-08-26
# by Michael Pal
#
#####################################################################################################################

# Parameters passed to the script

param([string]$from,[string]$to,[string]$subject,[string]$body)



# Server Name
$server = $env:computername

# Sender
if (!$from) {
    $from = "bops.systems@exchange.ubc.ca"
}

# Recipients List
if (!$to) {
    $to = "michael.pal@ubc.ca"
}

# Subject and Body
if (!$subject -and !$body) {
    $subject = "Unspecified error or possible hardware failure on $server"
    $body = "Unspecified error or possible hardware failure on $server. `n`n Please activate -subject and -body flags for more informational messages."
} 
elseif (!$subject) {
    $subject = "Unspecified error or possible hardware failure on $server"
    
    # Body
    if (!$body) {
        $body = "Unspecified error or possible hardware failure on $server"
    }
}

# Body
if (!$body) {
    $body = "Unspecified error or possible hardware failure on $server"
}

# SMTP Server
$smtpServer = "mail-relay.ubc.ca"

# Create new SMTP object to send email
$smtp = new-object Net.Mail.SmtpClient($smtpServer)

# Send the Email
$smtp.Send($from, $to, $subject, $body)
