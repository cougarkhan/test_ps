$fileContents = import-csv A:\temp\dsx\final.csv
$ascii = @()
$x = 0
$student = "Zoology-Student-No Updates"

$day = (get-date).day.tostring()
if ($day.length -eq 1) { $day = "0$day" }

$month = (get-date).month.tostring()
if ($month.length -eq 1) { $month = "0$month" }

$year = (get-date).year

$acls = @("1310","0330","3312","3322","3332","4312","4322","4332","3234","3244A","2244","1210","1230","1320C","1300","1330B","2320B","3320B","4220A","4230A","3220A","3240A","2210C","2230A","2230B","2280C","1280A","1260B","2332","4210B","4220B","4243B","4242","4242F","3204","3224","2214","2224","2234")

foreach ($item in $fileContents) {
    $ascii += $x
    $ascii += "I L76 U1 ^" + $item.IclassCrdNumber + "^^^"
    $ascii += "T Names"
    $ascii += "F FName ^" + $item.FirstName + "^^^"
    $ascii += "F LName ^" + $item.LastName + "^^^"
    $ascii += "F Company ^" + $item.CompanyName + "^^^"
    $ascii += "W"
    $ascii += "T UDF"
    $ascii += "F UdfNum ^1^^^"
    $ascii += "FUdfText ^" + $item.IclassCrdNumber + "^^^"
    $ascii += "W"
    $ascii += "T UDF"
    $ascii += "F UdfNum ^3^^^"
    $ascii += "F UdfText ^" + $item.EmployeeID + "^^^"
    $ascii += "W"
    $ascii += "T Cards"
    $ascii += "F Code ^" + $item.IclassCrdNumber + "^^^"
    $ascii += "F CardNum ^" + $item.IclassCrdNumber + "^^^"
    $ascii += "F StopDate ^" + $month + "/" + $day + "/" + $year.tostring() + "^^^"
    if ($item.CompanyName -match $student) {
        $ascii += "F StopDate ^" + $month + "/" + $day + "/" + ($year + 5).tostring() + "^^^"
    } else {
        $ascii += "F StopDate ^12/31/9999^^^"
    }
    foreach ($acl in $acls) {
        if ($item.$acl -eq "X") {
            $item.$acl
        }
    }
    
    
    $ascii += "`n`n"
    $x++
    $ascii
}