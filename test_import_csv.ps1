$mg = import-csv A:\Temp\m\page1.csv -delimiter :
$header = "Subject, Start Date, Start Time, Private, All Day Event, Description"

foreach ($thing in $mg) {
    $paymentNum = $thing.'Payment_#'
    $paymentDate = $thing.Payment_Date
    $interest = $thing.Interest
    $principal = $thing.Principal
    $balance = $thing.Balance
    $totalInterest = $thing.Total_Interest
    $totalPayment = [int]$interest + [int]$principal
    
    Write-Host $paymentNum $paymentDate $interest $principal $balance $totalInterest "`t" $totalPayment 
}