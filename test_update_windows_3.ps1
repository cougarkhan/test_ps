########################################################################################################
#
# Check for Updates
#
########################################################################################################

if ($updateSession = new-object -com Microsoft.update.Session) {
	Write-Host "Update Session object created..."
	$updateSearcher = $updateSession.CreateUpdateSearcher()
	$searchParameters = "IsInstalled=0 and ISHidden=0"
	$searchResult = $updateSearcher.search($searchParameters)
} else {
	Write-Host "Update Session object could not be created..."
	exit
}

if ($updatesToInstall = new-object -com Microsoft.update.UpdateColl) {
	Write-Host "Update Collection object created...`n"
} else {
	Write-Host "Update Collection object could not be created..."
	exit
}

if ($updateInstaller = $updateSession.CreateUpdateInstaller()) {
	Write-Host "Update Installer object created..."
} else {
	Write-Host "Update Installer object could not be created..."
	exit
}

if ($searchResult.Updates.Count -le 0) {
	Write-Host "System is Up to Date."
	exit
}

$tableFormat1 = @{Expression={"{0:N2}" -f ($_.MaxDownloadSize / (1024*1024)) + " MB"};Label="Download Size"}, @{Expression={"KB" + $_.KBArticleIDs};Label="KB Article"}, @{Expression={$_.Title};Label="Title";width=25}
				
Write-Host "`n`nThe following updates are available for download...`n"
$searchResult.Updates | FT $tableFormat1 -autosize

[int]$downloadSize = 0

foreach ($item in $searchResult.Updates) {
	$downloadSize += [int]$item.MaxDownloadSize
}

$downloadSize = $downloadSize / (1024*1024)

Write-Host "`nTotal Download Size is $downloadSize MB`n"

Write-Host "`nInstalling Updates...`n"

########################################################################################################
#
# Install Updates
#
########################################################################################################

foreach ($update in $searchResult.Updates) {
	$updatesToInstall.Clear()
	$result = $updatesToInstall.Add($update)
	$updateInstaller.Updates = $updatesToInstall
	
	Write-Host "Instaling $($update.Title)..."
	#$installationResult = $updateInstaller.Install()
	#Write-Host "`nInstallation Result $($installationResult.ResultCode)`n"
}






