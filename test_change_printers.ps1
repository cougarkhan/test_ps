$PACComputers = gc A:\Temp\PARKING\parking-computers.txt

#$PACComputers
#$wait = read-host

foreach ($computer in $PACComputers) {
    $result = gwmi -class Win32_PingStatus -filter "Address='$computer'"

    if ($result.StatusCode -eq 0) {
        Write-Host "$computer is " -nonewline
        Write-Host "Online" -foregroundcolor "GREEN" -nonewline
        Write-Host " " + $result.StatusCode
        $printers = gwmi -class WIn32_Printer -computer $computer
        
        foreach ($printer in $printers) {
            Write-Host "`t" $printer.Name
        }
    } else {
        Write-Host "$computer is " -nonewline
        Write-Host "Offline" -foregroundcolor "RED" -nonewline
        Write-Host " " + $result.StatusCode
    }
}