$error.clear()

function AddPrinters {
    trap {
        continue;
    }

    $printers = @("\\BOPS-FILE1\BuyerSide","\\BOPS-FILE1\HPLJ3055","\\BOPS-FILE1\HPLJ4100")

    $WSHNetwork = new-object -com WScript.Network
    foreach ($printer in $printers) {
        Write-Host Adding $printer.Name ... -nonewline
        if ($WSHNetwork.AddWindowsPrinterConnection($printer)) {
        $i
            Write-Host " Done!"
        } else {
            Write-Host " Failed!"
        }
    }
}

AddPrinters -ea SilentlyContinue

$error